(** A module that provides a runtime representation of refactorings. *)

open Containers
open Lib

module type S = sig

  type t =
    | AlphaRename of Identifier._t * int * int * string 
    | Identity
    | Rename of Identifier._t * string
    | Reduce of unit
    | Addarg of Identifier._t * string * string * string
    | Swaparg of Identifier._t * int * int
    | Inline of Identifier._t  * bool * Codebase.t
    (** The type of refactoring representations *)

  val pp : t Format.printer
  (** Pretty printer for representations. *)

  val to_string : t -> string
  (** Convert a representation to a string. *)

  val compare : t -> t -> int
  (** Compare two representations *)

  val equal : t -> t -> bool
  (** Test two representations for equality. *)

  module Set : SetWithMonoid.S with type elt = t
  (** Sets of refactoring representations *)

  val identity : t
  (** Representation of the identity refactoring *)

end
(** The signature for refactoring representations, of which this module is the
    canonical implementation. *)

include S
