open Containers

open Elements
open Elements.Base

open Identifier
open Refactoring_error

open Refactoring_visitors
open Refactoring_utils

open Compiler
open   Typedtree
open Ast_visitors

exception Unhandled_pattern

module ParameterState = struct

  module type S = sig
    type kind_t
    val kind : kind_t Base.t
    val initialise : string list -> unit
    val init : unit -> unit
    val to_repr : unit -> Refactoring_repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make  (X : sig
      type kind_t
      val kind : kind_t Base.t
    end)
    : S with type kind_t = X.kind_t = struct

    type kind_t = X.kind_t

    let kind = X.kind

    let init () = ()

    let initialise args = ()
    (** Initialise the refactoring using command-line arguments. *)

    let to_repr () =
      Refactoring_repr.Reduce ()

    let is_local input = true

    let kernel c =
      let c = Codebase.filter is_local c in
      if Codebase.is_empty c then
        let err =
          KernelError
            (Format.sprintf "kernel for %a is empty"
               Refactoring_repr.pp (to_repr ())) in
        raise (Error (err, None))
      else
        Codebase.to_list c

    let kernel_mem f = is_local f

  end

end

let shift_loc (l:Location.t) offset_x offset_y shift =
  let line = l.loc_start.pos_lnum + offset_y in
  let line_end = l.loc_end.pos_lnum + offset_y in
  let new_start =
    {l.loc_start with
     pos_cnum = if line = 1 then
         begin if not shift then
             (l.loc_start.pos_cnum - l.loc_start.pos_bol + offset_x)
           else
             l.loc_start.pos_cnum + offset_x - 1
         end
       else l.loc_start.pos_cnum
     ;
     pos_lnum = l.loc_start.pos_lnum + offset_y;
     pos_bol = if line = 1 && not shift then 1 else l.loc_start.pos_bol  } in
  let new_end =
    {l.loc_end with
     pos_cnum =
       if line_end = 1 then
         begin if not shift then
             (l.loc_end.pos_cnum - l.loc_end.pos_bol + offset_x)
           else
             l.loc_end.pos_cnum + offset_x -1
         end
       else l.loc_end.pos_cnum;
     pos_lnum = l.loc_end.pos_lnum + offset_y;
     pos_bol = if line_end = 1 && not shift then 1 else l.loc_end.pos_bol} in
  {l with loc_start = new_start; loc_end = new_end}



module Make () = struct

  module InState = InputState.Make ()
  module Param = ParameterState.Make (struct
      type kind_t = _structure
      let kind = Elements.Base.Structure
    end)
  include Param

  let name = "reduce"

  let get_deps ~mli source = Refactoring_deps.Elt.Set.empty

  module Reps = struct

    module StringSet =
    struct
      module StringSet = Set.Make(String)
      include StringSet
      class monoid = object
        inherit [t] VisitorsRuntime.monoid
        method private zero = empty
        method private one x = StringSet.singleton x
        method private plus = union
      end
    end

    module LocStrList =
    struct
      class monoid = object
        method private zero = []
        method private one x = [x]
        method private plus x y = x@y
      end
    end

    class ['self] result_reducer_string = object (self : 'self)
      inherit ['self] Typedtree_visitors.reduce
      inherit StringSet.monoid
    end


    class ['self] result_reducer_locstr = object (self : 'self)
      inherit ['self] Typedtree_visitors.reduce
      inherit LocStrList.monoid
    end


    (* Get name of variables in patterns  *)
    let visitor_pattern_vars = object(self)
      inherit [_] result_reducer_string as super
      method visit_Tpat_var () id strloc =
        self#one strloc.txt
    end

    (* Get location and name of variables in patterns  *)
    let visitor_pattern_locvars = object(self)
      inherit [_] result_reducer_locstr as super
      method visit_Tpat_var () id strloc =
        self#one (strloc.loc,strloc.txt)
    end

    (* Get set of free variable names in expressions :  *)
    let visitor_free_vars = object(self)
      inherit [_] result_reducer_string as super

      method visit_Texp_ident () path lidloc vd =
        self#one (Ast_lib.longident_to_string lidloc.txt)

      method visit_Texp_let () rflag vbindings exp =
        let fv = self#visit_tt_expression () exp in
        let free_vars_vb vb binders fvars =
          let binds = visitor_pattern_vars#visit_tt_pattern () vb.vb_pat in
          (StringSet.to_list binds@binders,
           StringSet.union fvars (self#visit_tt_expression () vb.vb_expr))
        in
        let (binders,fvars) =
          List.fold_left
            (fun (binders,fvars) vb -> free_vars_vb vb binders fvars)
            ([], StringSet.empty) vbindings in
        List.fold_left
          (fun acc x -> StringSet.remove x acc)
          (StringSet.union fv fvars) binders

      method visit_Texp_function () _ param cases _ =
        let rec free_vars_case c =
          match c.c_lhs.pat_desc with
          | Tpat_var (id,stringloc) ->
            StringSet.remove stringloc.txt (self#visit_tt_expression () c.c_rhs)
          | _ -> self#visit_tt_expression () c.c_rhs in
        let fvs = List.fold_left
            (fun acc c -> StringSet.union acc (free_vars_case c))
            StringSet.empty
            cases in
        StringSet.remove (Ident.name param) fvs

      method visit_Texp_try () e cases =
        let free_vars_case {c_lhs; c_guard; c_rhs } =
          let vars = visitor_pattern_vars#visit_tt_pattern () c_lhs in
          StringSet.diff vars (self#visit_tt_expression () c_rhs)
        in
        let fvcs = List.fold_left
            (fun acc x -> StringSet.union acc (free_vars_case x))
            StringSet.empty cases in
        StringSet.union (self#visit_tt_expression () e) fvcs

      method visit_Texp_for () _ patt e1 e2 _ e3 =
        let fv1 = self#visit_tt_expression () e1 in
        let fv2 = self#visit_tt_expression () e2 in
        let fv3 =
          match patt.ppat_desc with
          | Ppat_var x ->
            StringSet.remove x.txt (self#visit_tt_expression () e3)
          | _ -> self#visit_tt_expression () e3
        in
        StringSet.union (StringSet.union fv1 fv2) fv3

      method visit_tt_expression () e =
        match e.exp_desc with
        (* Trick to be compatible with OCaml versions < 4.07 *)
        | (Texp_match _) as texp ->
          let e = Typedtree.get_match_expression texp in
          let cases = Typedtree.get_match_cases texp in
          let free_vars_case {c_lhs; c_guard; c_rhs } =
            let vars = visitor_pattern_vars#visit_tt_pattern () c_lhs in
            StringSet.diff vars (self#visit_tt_expression () c_rhs)
          in
          let fvcs =
            List.fold_left
              (fun acc x -> StringSet.union acc (free_vars_case x))
              StringSet.empty cases in
          StringSet.union (self#visit_tt_expression () e) fvcs
        | _ -> super#visit_tt_expression () e

    end


    (* Return location of every variables = id in an expression  *)
    let rec visitor_repls_locations free_vars id to_str = object(self)
      inherit [_] result_reducer_locstr as super

      method visit_Texp_ident () path lidloc vd =
        let name = Ast_lib.longident_to_string lidloc.txt in
        if String.equal name id then
          self#one (lidloc.loc,to_str)
        else
          self#zero

      method visit_tt_expression () e =
        match e.exp_desc with
        (* Trick to be compatible with OCaml versions < 4.07  *)
        | (Texp_match _) as texp ->
          let param_is_free_var case =
            let vars = visitor_pattern_vars#visit_tt_case () case in
            let vars = StringSet.to_list vars in
            List.fold_left (fun acc s -> StringSet.mem s free_vars) false vars
          in
          let e = Typedtree.get_match_expression texp in
          let cases = Typedtree.get_match_cases texp in
          let get_vars_case {c_lhs; c_guard; c_rhs } =
            visitor_pattern_vars#visit_tt_pattern () c_lhs
          in
          let vars_cases = List.fold_left (fun acc case -> get_vars_case case)
              StringSet.empty cases in
          let el = self#visit_tt_expression () e in
          if List.fold_left (fun acc x -> acc || param_is_free_var x) false cases then 
            raise Unhandled_pattern;
          if StringSet.mem id vars_cases then el else
            let cl = List.fold_left
                (fun acc c -> self#plus acc (self#visit_tt_case () c))
                el cases in
            cl
        (* TODO : rename (like in let expressions) *)
        | _ -> super#visit_tt_expression () e


      method visit_Texp_let () rflag vbindings exp =
        let bound vb =
          let vars = visitor_pattern_vars#visit_tt_value_binding () vb in
          StringSet.mem id vars
        in
        let param_is_free_var vb =
          (* Get all names in pattern  *)
          let vars = visitor_pattern_vars#visit_tt_value_binding () vb in
          let vars = StringSet.to_list vars in
          (* Check if one of them is part of free variables  *)
          List.fold_left (fun acc s -> StringSet.mem s free_vars) false vars
        in
        let visit_vb vb acc = self#visit_tt_expression () vb.vb_expr in
        let vblocs = List.fold_left (fun acc x -> visit_vb x acc) [] vbindings in
        let shadowed = List.fold_left (fun acc x -> acc || bound x) false vbindings in
        (* if param <> from and param ∉ FV(_to) :
           \param.cases[from := to] *)
        if shadowed then
          vblocs
        else
          let rename_binder vb fv exp =
            let vars = visitor_pattern_locvars#visit_tt_value_binding () vb in
            let repls = List.fold_left (fun acc (loc,name) ->
                if StringSet.mem name fv then
                  let fresh_name = name^"'" in
                  (loc,name,fresh_name) :: acc
                  (* todo : real fresh name  *)
                  (* raise Unhandled_pattern *)
                else
                  acc
              ) [] vars
            in
            let le = List.fold_left (fun acc (loc,name,fresh_name) ->
                let v = visitor_repls_locations (StringSet.empty) name fresh_name in
                self#plus (v#visit_tt_expression () exp) acc
              ) [] repls in
            let repls = List.map (fun (loc,_,fresh_name) -> (loc,fresh_name)) repls in
            self#plus repls le
          in
          if List.fold_left (fun acc x -> acc || param_is_free_var x) false vbindings then
            let repls' = List.fold_left (fun acc vb ->
                rename_binder vb free_vars exp @ acc) [] vbindings in
            self#plus repls' (self#visit_tt_expression () exp)
          else
            self#plus vblocs (self#visit_tt_expression () exp)

      method visit_Texp_for () id' patt e1 e2  dir e3 =
        let name = Ident.name id' in
        let shadowed = String.equal name id in
        let le2 = self#visit_tt_expression () e2 in
        let le1 = self#visit_tt_expression () e1 in
        if shadowed then
          self#plus le1 le2
        else
          self#plus (self#plus le1 le2) (self#visit_tt_expression () e3)


    end



    let visitor = object(self)



      inherit [_]  result_reducer as super
      method! visit_tt_expression ctx this =
        try
          match this.exp_desc with
          | Texp_apply ({ exp_desc = Texp_function { cases; _} ; exp_attributes } ,args) ->
            begin match exp_attributes with
              | [attr] when String.equal (get_attribute_name attr) "rotor_reduce" ->
                (* Since the lambda is curried we need to visit it to get the body of the function  *)
                let rec get_lambda_body e =
                  match e.exp_desc with
                  | Texp_function { cases; _} ->
                    begin match cases with
                      | [case] ->
                        get_lambda_body case.c_rhs
                      | _ -> assert false
                    end
                  | _ -> e
                in
                (* Returns list of parameter * argument *)
                let rec get_param_arg_couples e =
                  match e.exp_desc with
                  | Texp_apply (e ,args) ->
                    let rec aux e args =
                      match e.exp_desc with
                        Texp_function { cases; _} ->
                        begin match cases with
                          | [case] ->
                            begin match case.c_lhs.pat_desc with
                              | Tpat_var(id,strloc) ->
                                let param = strloc.txt in
                                begin match args with
                                  | [] -> (param,None,None)::(aux case.c_rhs args)
                                  | (label, Some arg)::args ->
                                    let str_arg = Sourcefile.extract_location (InState.get_input ()) arg.exp_loc in
                                    let s = self#visit_tt_expression () arg in
                                    let col = arg.exp_loc.loc_start.pos_cnum - arg.exp_loc.loc_start.pos_bol in
                                    let lin = arg.exp_loc.loc_start.pos_lnum in
                                    let offset_x = -1 * col + 1 in
                                    let offset_y = -1 * lin + 1  in
                                    let s = Replacement.Set.map (fun r ->
                                        let loc = Replacement.location r in
                                        let payload = Replacement.payload r in
                                        let new_loc = shift_loc loc offset_x offset_y false in
                                        Replacement.mk new_loc payload) s in
                                    let str_arg' = Replacement.apply_all s str_arg in
                                    (param,Some str_arg', Some arg)::(aux case.c_rhs args)
                                  | (label, None)::_ -> assert false
                                end
                              | Tpat_any -> aux case.c_rhs (List.tl args)
                              | Tpat_construct(_,_,[]) -> aux case.c_rhs (List.tl args)
                              (* For the moment we cannot handle other patterns (e.g. tuples) in parameters *)
                              | _ -> raise Unhandled_pattern
                            end
                          | _ -> assert false
                        end
                      | _ ->  []
                    in
                    aux e args
                  | _ -> failwith "not a lambda application"
                in
                let pae = get_param_arg_couples this in

                let rec safety_check l =
                  let rec aux from _to l =
                    match l with
                    | [] -> ()
                    | ( param,_,_)::xs ->
                      if not (String.equal param from) && StringSet.mem param (visitor_free_vars#visit_tt_expression () _to) then
                        failwith "Name clash"
                      else
                        aux from _to xs
                  in
                  match l with
                  | [] -> ()
                  | ( from,Some _,Some _to)::xs ->
                    aux from _to xs ;
                    safety_check xs
                  | _ -> ()
                in
                safety_check pae;

                (* for each (from,to) in param_args:
                     for each following (param,arg')
                      if param <> from and param in FV(to):
                        fail *)

                let (body,repls) = match this.exp_desc with
                  | Texp_apply ({ exp_desc = Texp_function { cases; _}} as e ,args) ->
                    let body = get_lambda_body e in
                    let col = body.exp_loc.loc_start.pos_cnum - body.exp_loc.loc_start.pos_bol in
                    let lin = body.exp_loc.loc_start.pos_lnum in
                    let offset_x = -1 * col + 1 in
                    let offset_y = -1 * lin + 1  in
                    let params_and_body_shifted = e.exp_loc.loc_start.pos_lnum <> lin in
                    let str = Sourcefile.extract_location (InState.get_input ()) body.exp_loc in
                    let get_repls_for_param p a e =
                      let fv = visitor_free_vars#visit_tt_expression () e in
                      let locs = (visitor_repls_locations fv p a)#visit_tt_expression () body in
                      let open Location in
                      let locs = List.map (fun (loc,s) -> (shift_loc loc offset_x offset_y params_and_body_shifted,s) ) locs in
                      List.map (fun (l,s) -> Replacement.mk l s) locs
                    in
                    let pae' = List.fold_left (fun acc (p,a,e) ->
                        begin match a,e with
                          | Some a, Some e -> (p,a,e)::acc
                          | _ -> acc
                        end
                      ) [] pae in
                    str,List.fold_left (fun acc (p,a,e) -> get_repls_for_param p a e @ acc) [] pae'
                  | _ -> failwith "no lambda here"
                in
                let repls =
                  List.fold_left
                    (fun acc x -> Replacement.Set.union (Replacement.Set.singleton x) acc)
                    Replacement.Set.empty repls
                in
                let non_applied_args = List.fold_left (fun acc (p,a,e) ->
                    match a,e with
                    | None, None -> p::acc
                    | _ -> acc
                  ) [] pae
                in
                let new_str = Replacement.apply_all repls body in
                let prefix = match non_applied_args with
                  | [] -> ""
                  | _ -> let s = String.concat " " (non_applied_args) in
                    Format.sprintf "fun %s -> " s
                in
                let new_str = Format.sprintf "(%s%s)" prefix new_str in
                Replacement.Set.singleton (Replacement.mk this.exp_loc new_str)
              | _ ->
                super#visit_tt_expression () this
            end
          | _ ->
            super#visit_tt_expression ctx this
        with Unhandled_pattern -> self#zero

    end
  end

  let process_struct scope _struct =
    let replacements =
      Reps.
        (visitor#visit_tt_structure () _struct) in
    let () = if Replacement.Set.is_empty replacements then
        Logging.warn @@ fun _f -> _f
          "Could not find local value to extend in %s!"
          (InState.get_input ()).fileinfos.filename in
    replacements

  let process_file input =
    let open Sourcefile in
    let open Fileinfos in
    let infos = input.fileinfos in
    let mod_scope = ((module_name infos, None), []) in
    let () = InState.set_input input in
    InState.dispatch_on_input_type
      ~intf_f:(fun _ -> Replacement.Set.empty)
      ~impl_f:(process_struct mod_scope)

end
