open Containers
open   List
open   Fun

open Compiler
open   Asttypes
open   Ident
open   Longident
open   Typedtree
open   Types
open   Compmisc

open Ast_visitors
open   Visitors_lib

open Refactoring_deps
open Refactoring_error
open Refactoring_repr
open Refactoring_utils
open Refactoring_visitors

open Lib
open Ast_lib
open Ast_utils
open Compiler_utils

open Elements.Base
open Identifier

open Typedtree_views

open Sourcefile
open Fileinfos

open Logging.Tags

open Inline_core



module DepSet = Refactoring_deps.Elt.Set

module Make () = struct

  module InState = InputState.Make ()
  module Param = ParameterState.Make (struct
      type kind_t = _value
      let kind = Elements.Base.Value
    end)
  include Param

  (*** Properties ***)

  let name = "Swap arguments implementation value"


  let fun_name_as_str () =
    let Atom.Data.{ id; _ } =
      Atom.unwrap (Chain.anchor (fun_id ())) in
    id

  (* ------------------------------------------------------------------------
      REPLACEMENTS
     ------------------------------------------------------------------------- *)
  module Reps = struct

    include Inline_visitors.Make (InState) (Param)

  end


  let lookup_head_ident env cont =
    let from_id = fun_id () in
    let Atom.Ex hd = Chain.hd from_id in
    let path =
      lookup_path
        ~lookup_f:(fun lid -> Env.lookup_module ~load:true lid env)
        ?backup_lib:((InState.get_input ()).fileinfos.library)
        ~env
        (fun_lib (), Longident.Lident Atom.((unwrap hd).Data.id)) in
    match path with
    | None ->
      cont None
    | Some ((Path.Pident hd) as path) ->
      let { md_type; _ } = Env.find_module path env in
      let item =
        Moduletype.resolve_lookup env md_type (Chain.tl_exn from_id) in
      cont (Option.map (fun _ -> hd) item)
    | _ ->
      assert false

  let process_nonlocal env cont =
    lookup_head_ident env (Option.map_or ~default:Replacement.Set.empty cont)

  let process_sig local scope _sig =
    if local then
      let from_id = Chain.tl_exn (fun_id ()) in
      Reps.Local.(visitor#visit_tt_signature (new env from_id scope) _sig)
    else
      Replacement.Set.empty

  let process_struct local scope _struct =
    if local then
      let from_id = Chain.tl_exn (fun_id ()) in
      let replacements =
        Reps.Local.
          (visitor#visit_tt_structure (new env from_id scope) _struct) in
      let () = if Replacement.Set.is_empty replacements then
          (* TODO: Consider: do we need to emit this warning? *)
          Logging.warn @@ fun _f -> _f
            "Could not find local value to extend in %s!"
            (InState.get_input ()).fileinfos.filename in
      replacements
    else
      let modid = Modulescope.to_identifier scope in
      process_nonlocal
        (initial_env (Str _struct))
        (fun ident ->
           let visitor = Reps.Nonlocal.visitor (ident, fun_id (),  modid) in
           visitor#visit_tt_structure `NO_PUN _struct)

  let process_file input =
    let infos = input.fileinfos in
    let local = is_local infos in
    let mod_scope = ((module_name infos, None), []) in
    let () = InState.set_input input in
    InState.dispatch_on_input_type
      ~intf_f:(process_sig local mod_scope)
      ~impl_f:(process_struct local mod_scope)

  (* ------------------------------------------------------------------------
     DEPENDENCIES
     ------------------------------------------------------------------------ *)

  module Deps = Inline_deps.Make (InState) (Param)

  let get_deps_nonlocal env cont =
    lookup_head_ident env (Option.map_or ~default:DepSet.empty cont)

  let get_sig_deps local scope _sig = DepSet.empty

  let get_struct_deps local scope _struct =
    if local then
      let from_id = Chain.tl_exn (fun_id ()) in
      Deps.Local.
        (visitor#visit_tt_structure (new env from_id scope None) _struct)
    else
      get_deps_nonlocal
        (initial_env (Str _struct))
        (fun ident ->
           let visitor = Deps.Nonlocal.visitor (ident, fun_id ()) in
           visitor#visit_tt_structure scope _struct)

  let get_deps ~mli input =
    let infos = input.fileinfos in
    let local = is_local infos in
    let mod_scope =
      Option.map_or
        ~default:((module_name infos, None), []) initial_module_scope mli in
    let () = InState.set_input input in
    let deps =
      InState.dispatch_on_input_type
        ~intf_f:(get_sig_deps local mod_scope)
        ~impl_f:(get_struct_deps local mod_scope) in
    let _ = InState.get_input () in
    let () =
      if not (DepSet.is_empty deps) then
        Logging.info @@ fun _f -> _f
          ~header:"Deps:"
          "@[<v>%a@]" (DepSet.pp Refactoring_deps.Elt.pp) deps in
    deps
end
