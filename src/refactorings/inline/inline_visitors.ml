open Containers
open   List
open   Fun


open Compiler
open   Asttypes
open   Ident
open   Longident
open   Typedtree
open   Types
open   Compmisc

open Ast_visitors
open   Visitors_lib

open Refactoring_deps
open Refactoring_error
open Refactoring_repr
open Refactoring_utils
open Refactoring_visitors

open Lib
open Ast_lib
open Ast_utils
open Compiler_utils

open Elements.Base
open Identifier

open Typedtree_views

open Sourcefile
open Fileinfos

open Logging.Tags

open Inline_core


module Make(InState:InputState.S) (Param:ParameterState.S) = struct
  include Param

  module rec Nonlocal : sig
    include module type of struct
      let visitor
        : Ident.t * _value Chain.t * Chain._t option ->
          < visit_tt_structure
            : [ `ARG_PUN | `FIELD_PUN | `NO_PUN ] -> _ ; .. > =
        fun _ ->
          object (self)
            inherit [_] result_reducer
          end
    end
  end = struct


    let visitor (ident, from_id,moduleid) =
      object(self)

        inherit [_] result_reducer as super

        method! visit_tt_structure_item pun_tag ({ str_desc ; _ } as this)=
          match str_desc with
          | Tstr_value (r,{vb_pat = {pat_desc = Tpat_var (id,s) ; _ } ;_ }::xs)
            when Ident.same id ident  ->
            (* This prevents recursive inlinings  *)
            Replacement.Set.empty
          | _ ->
            super#visit_tt_structure_item pun_tag this

        method! visit_tt_expression pun_tag
            ({ exp_env; exp_loc; exp_desc; exp_extra; _ } as this) =
          let env =
            find_map
              (fun (x,_,_) -> env_of_exp_extra x)
              (List.rev exp_extra) in
          let env = Option.get_or ~default:exp_env env in
          match exp_desc with
          | Texp_ident (p, { txt; loc; }, _) ->
            let p =
              try
                Env.normalize_module_path (Some loc) env p
              with ((Env.Error e) as err) ->
                let () = Logging.err @@ fun _f -> _f
                    "Error normalizing path %a: %a"
                    Printtyp.path p
                    Env.report_error e in
                raise err
            in
            let head = Path.head p in
            let p =
              if Ident.persistent head then snd (Buildenv.Factorise.path p)
              else p in
            let id =
              Chain.of_path Elements.Base.Value p in
            if not ((Ident.same ident head) && Chain.equal id from_id) then
              self#zero
            else
              begin try
                  let str =
                    match pun_tag with
                    | `NO_PUN ->
                      let deps = dependencies_with_path () in
                      let modid = Option.get_exn moduleid in
                      let destroy_chain c =
                        let Chain.Ex (c,d) = c in
                        let h = Chain.to_longident d in
                        Longident.flatten h
                      in
                      let modl = destroy_chain modid in
                      let remove_prefix_dot s =
                        if Char.equal s.[0] '.' then
                          String.sub s 1 (String.length s -1)
                        else
                          s
                      in
                      (* Remove a path from a longident *)
                      let rec remove p l id =
                        match p,l with
                        | x::xs , y::ys when String.equal x y ->
                          remove xs ys id
                        | _ -> l

                      in
                      let compute_substitution (d,p) =
                        let open ParameterState in
                        let s = Path.to_longident p |> Longident.flatten in
                        let full_str = String.concat "." (remove modl s d.id) in
                        let full_str = remove_prefix_dot
                            (full_str^"."^(Ident.name d.id)) in
                        (d.id,full_str)
                      in
                      let deps = List.map compute_substitution deps in
                      let deps = List.sort_uniq
                          ~cmp:(fun (x,l) (y,l) -> Ident.compare x y) deps in
                      let body = fun_body () in
                      let body = List.fold_left
                          (fun acc (x,l) ->
                             String.replace ~sub:(Ident.unique_name x)
                               ~by:l acc) body deps in
                      body
                    | `ARG_PUN ->
                      failwith "Arg Pun"
                    | `FIELD_PUN ->
                      failwith "Field Pun"
                  in
                  Replacement.Set.of_opt (Replacement.mk_opt loc str)
                with _ -> Replacement.Set.empty
              end
          | Texp_apply (_fun, args) ->
            (* Get the replacement set for the function part *)
            let result = self#visit_tt_expression pun_tag _fun in
            let process_arg prev_loc ((_, exp) as arg) =
              match exp.exp_desc with
              | Texp_ident _ when not exp.exp_loc.Location.loc_ghost ->
                (* Set the correct pun tag and get replacement set for arg *)
                begin try
                    let preamble = extract_src (InState.get_input ())
                        prev_loc
                        exp.exp_loc in
                    let pun_tag =
                      if is_arg_pun preamble arg then `ARG_PUN else `NO_PUN in
                    self#visit_tt_expression pun_tag exp
                  with Invalid_argument s ->
                    let () = Logging.warn @@ fun _f -> _f
                        "@[<v 4>%s@,%a@,%a@]" s
                        Location.print_loc prev_loc
                        Location.print_loc exp.exp_loc in
                    Replacement.Set.empty
                end
              | _ -> self#visit_tt_expression pun_tag exp in
            (* Remove None args *)
            let args =
              List.filter_map
                (fun (lbl, exp) -> (Option.map (Pair.make lbl) exp))
                args in
            (* sort by location *)
            let args =
              List.sort
                (fun (_, exp) (_, exp') -> cmp_loc exp.exp_loc exp'.exp_loc)
                args in
            let result, args = match args with
              | [] ->
                result, args
              | ((_, exp) as arg) :: args ->
                (* only apply to args that come after the function *)
                if cmp_loc exp.exp_loc _fun.exp_loc < 0 then
                  self#plus result (process_arg exp_loc arg), args
                else
                  result, arg::args in
            (* Fold to a set of replacements  *)
            let result, _ =
              List.fold_left
                (fun (result, prev_loc) ((_, exp) as arg) ->
                   (self#plus result (process_arg prev_loc arg)),
                   (if exp.exp_loc.Location.loc_ghost
                    then prev_loc else exp.exp_loc))
                (result, _fun.exp_loc) args in
            result
          | Texp_record { fields; extended_expression; _ } ->
            let result = match extended_expression with
              | Some exp -> self#visit_tt_expression pun_tag exp
              | None -> Replacement.Set.empty in
            (* Check for punning when processing record fields
               ----------------------------------------------------------------
               The type checker fills in missing fields and rearranges them
               to match the order they are given in the record definition. So
               we need to filter out the ones not given in the source, and then
               sort by location so that we can correctly compute each field's
               preamble in the source text to check for punning. *)
            let process_fld prev_loc ((_, (_, exp)) as fld) =
              match exp.exp_desc with
              | Texp_ident _ when not exp.exp_loc.Location.loc_ghost ->
                begin try
                    let preamble =
                      extract_src (InState.get_input ())
                        prev_loc exp.exp_loc in
                    let pun_tag =
                      if is_field_pun preamble fld
                      then `FIELD_PUN else `NO_PUN in
                    self#visit_tt_expression pun_tag exp
                  with Invalid_argument s ->
                    let () = Logging.warn @@ fun _f -> _f
                        "@[<v 4>%s@,%a@,%a@]" s
                        Location.print_loc prev_loc
                        Location.print_loc exp.exp_loc in
                    Replacement.Set.empty
                end
              | _ -> self#visit_tt_expression pun_tag exp in
            let fields = Array.fold_right
                (fun (lbl_desc, lbl_def) acc -> match lbl_def with
                   | Kept _ -> acc
                   | Overridden (loc, exp) -> (lbl_desc, (loc, exp))::acc)
                fields [] in
            let fields = List.sort
                (fun (_, (_, exp)) (_, (_, exp')) ->
                   cmp_loc exp.exp_loc exp'.exp_loc) fields in
            let result, _ = List.fold_left
                (fun (result, prev_loc) ((_, (_, exp)) as lbl) ->
                   (self#plus result (process_fld prev_loc lbl)), exp.exp_loc)
                (result,
                 (Option.map_or ~default:exp_loc (fun exp -> exp.exp_loc)
                    extended_expression))
                fields in
            result
          | _ ->
            super#visit_tt_expression pun_tag this

        method! visit_tt_include_declaration env decl =
          let { incl_mod; incl_loc; incl_type; _ } = decl in
          begin match Module.unwind_expr incl_mod with
            | { mod_desc = Tmod_ident (p, _); _ } as me, apps, _ ->
              let p = Env.normalize_module_path (Some me.mod_loc) me.mod_env p
              in
              let head = Path.head p in
              let p =
                if Ident.persistent (Path.head p)
                then snd (Buildenv.Factorise.path p)
                else p in
              if Ident.same ident head then
                let sort =
                  if List.is_empty apps
                  then Elements.Base.(Ex Structure)
                  else Elements.Base.(Ex Functor) in
                let Chain.Ex (_, p) = Chain._of_path sort p in
                begin match Chain.drop p from_id with
                  | None ->
                    (* [p] should not be equal to [from_id] since the latter
                       refers to a value and the former to a module. *)
                    assert false
                  | _ -> ()
                  | exception Invalid_argument _ -> ()
                end
            | _ -> ()
          end ;
          let locals = self#process_functor_application incl_mod in
          let nonlocals = super#visit_tt_include_declaration env decl in
          self#plus locals nonlocals

        method! visit_tt_module_binding env ({ mb_expr; mb_id } as mb) =
          let locals = self#process_functor_application mb_expr in
          let nonlocals = super#visit_tt_module_binding env mb in
          self#plus locals nonlocals



        method private process_functor_application me = self#zero

        (* Inlining a function defined into a functor is complicated because the
           function can depend on functions that come from the parameters of the
           functor :

           functor F (X : val g ..) ->
             let r = ref 0
             let foo x = X.g 3 + !r
           end

           F.foo

           module Q = F (X)
           Q.foo -> A.g 3 + Q.!r

           module P = F (P)
           P.foo -> P.g 3 + P.!r

           let module M = if b then F (A) else F (B) in
           M.foo

           module N = G(X)
           module M = F(N)

           (* Inline foo from functor F (X : ...) -> struct ... let foo x = X.g 3 ... end  *)
           module Bar = F (...)
           (* dependes on inlining B:foo *)

           B.foo |-> (fun x -> ? 3) *)

        (* match Module.unwind_expr me with
           | { mod_desc = Tmod_ident (p, _); _ } as me, (_::_ as apps), tys ->
           let p =
            Env.normalize_module_path (Some me.mod_loc) me.mod_env p in
           let head = Path.head p in
           let p =
            if Ident.persistent (Path.head p)
            then snd (Buildenv.Factorise.path p)
            else p in
           if not (Ident.same ident head) then
            self#zero
           else
            let p = Chain.of_path Functor p in
            begin match Chain.drop p from_id with
              | None ->
                (* [p] should not be equal to [from_id] since the latter
                   refers to a value and the former to a module. *)
                assert false
              | Some ((Chain.InParameter (Indexed idx, tl)) as c)
                when idx < List.length apps ->
                List.foldi
                  (fun reps i (me, tys) ->
                     let reps =
                       if i < idx then
                         let c = Chain.demote ~by:(i+1) c in
                         let visit =
                           Local.visitor#visit_tt_module_type
                             (new Local.env c (Modulescope.empty)) in
                         List.fold_left (Fun.flip (visit %> self#plus))
                           reps
                           tys
                       else reps in
                     if i+1 = idx then
                       self#plus reps
                         Local.(
                           visitor#visit_tt_module_expr
                             (new env tl Modulescope.empty)
                             (me))
                     else reps)
                  (self#zero)
                  (apps)
              | _ ->
                self#zero
              | exception Invalid_argument _ ->
                self#zero
            end
           | _ ->
           self#zero *)
        (* N.B. No need to do nonlocal renaming in signatures. *)

      end
  end
  and

    Local : sig
    include module type of struct
      class ['a] env id scope = object (self)
        inherit ['a] id_env id
        inherit module_scope_env scope
      end
      let visitor : < visit_tt_structure : _value env -> _ ; .. > =
        object(self)
          inherit [_] reduce_with_id
          inherit Replacement.Set.monoid
        end
    end
  end = struct

    class ['a] env id scope = object (self)
      inherit ['a] id_env id
      inherit module_scope_env scope
    end

    let visitor =
      object(self)

        inherit [_] reduce_with_id as super
        inherit Replacement.Set.monoid

        method! process_binding_from_struct env this
            (type b)
            ((id : (Ident.t, b) Atom.t), _)
            (((_, _, InStr { str_env; str_loc; }, binding) as _source)
             : (b, impl) binding_source) =
          let locals =
            match id, binding with
            | _, `Binding Value (_, InStrPrim { val_name = { loc; _}; _ }) ->

              self#zero
            | Atom.Value id, `Binding Value (_, InStrVal { vb_pat; vb_expr }) ->
              self#zero
            | Atom.Value _, `Include InStr decl ->
              self#visit_tt_include_declaration env decl
            | _, `Binding Structure (_, InStr mb) ->
              self#visit_tt_module_binding env mb
            | _, `Binding StructureType InStr mtd ->
              self#visit_tt_module_type_declaration env mtd
            | _, `Binding Functor (_, InStr mb) ->
              self#visit_tt_module_binding env mb
            | _, `Binding FunctorType InStr mtd ->
              self#visit_tt_module_type_declaration env mtd
            | _, `Include InStr decl ->
              self#visit_tt_include_declaration env decl
          in
          let Atom.Data.{ id; _ } = Atom.unwrap id in
          let moduleid = Modulescope.to_identifier env#scope in
          let nonlocals =
            (Nonlocal.visitor (id, env#id,moduleid))#visit_tt_structure `NO_PUN
              {this with
               str_items =
                 let InStr rest = next_items _source in rest } in

          self#plus locals nonlocals

        method! visit_tt_module_binding env mb =
          let id = Chain.tl_exn env#id in
          let scope, _ =
            Modulescope.enter_module_binding (InStr mb) env#scope in
          super#visit_tt_module_binding (new env id scope) mb

        method! visit_tt_include_declaration env decl =
          let scope, _ = Modulescope.enter_include (InStr decl) env#scope in
          super#visit_tt_include_declaration (env#with_scope scope) decl

        method! visit_tt_module_expr env ({ mod_loc; _ } as me) =
          let me, apps, tys = Module.unwind_expr me in
          let body_reps =
            match me.mod_desc with
            | Tmod_apply _
            | Tmod_constraint _ ->
              assert false
            | Tmod_ident _ ->
              self#zero
            | Tmod_functor (x, _, x_type, body) ->
              self#zero
            | Tmod_structure _struct ->
              self#visit_tt_structure env _struct
            | Tmod_unpack _ ->
              let () = Logging.warn @@ fun _f -> _f
                  ~tags:(of_loc mod_loc)
                  "module bound to an expression unpacking - cannot do local \
                   renaming!" in
              self#zero in
          body_reps

        method! process_binding_from_sig env this
            (type b)
            ((id : (Ident.t, b) Atom.t), _)
            ((_,_, InSig { sig_env; _ }, binding)
             : (b, intf) binding_source) =

          Replacement.Set.empty


        method! visit_tt_module_declaration env md =
          let id = Chain.tl_exn env#id in
          let scope, _ =
            Modulescope.enter_module_binding (InSig md) env#scope in
          super#visit_tt_module_declaration (new env id scope) md

        method! visit_tt_module_type_declaration env mtd =
          let id = Chain.tl_exn env#id in
          let scope, _ =
            Modulescope.enter_module_type_declaration mtd env#scope in
          super#visit_tt_module_type_declaration (new env id scope) mtd

        method! visit_tt_include_description env desc =
          let scope, _ = Modulescope.enter_include (InSig desc) env#scope in
          super#visit_tt_include_description (env#with_scope scope) desc

        method! visit_tt_module_type env this =
          match this.mty_desc with
          | Tmty_typeof _ ->
            let () = Logging.warn @@ fun _f -> _f
                ~tags:(of_loc this.mty_loc)
                "module has type of a module expression - cannot do local \
                 renaming!" in
            self#zero
          | _ ->
            self#zero
      end
  end
end
