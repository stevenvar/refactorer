open Containers
open   List
open   Fun

open Elements
open Elements.Base

open Identifier
open Refactoring_error

open Compiler
open   Typedtree
open Ast_visitors

let fun_body_r : string option ref = ref None

module ParameterState = struct
  type dependency = { id : Ident.t;
                      path : Path.t ;
                      lid : Longident.t;
                      loc : Location.t }

  type scope = Path.t
  module type S = sig
    type kind_t
    val kind : kind_t Base.t
    val initialise : string list -> unit
    val init : kind_t Identifier.t * bool * Codebase.t -> unit
    val fun_lib : unit -> string option
    val fun_id : unit -> kind_t Chain.t
    val dependencies_with_path : unit -> (dependency * scope) list
    val mark_reduce : unit -> bool
    val codebase : unit -> Codebase.t
    val fun_body : unit -> string
    val to_repr : unit -> Refactoring_repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make
      (X : sig
         type kind_t
         val kind : kind_t Base.t
       end)
    : S with type kind_t = X.kind_t =
  struct

    type kind_t = X.kind_t

    let kind = X.kind

    let fun_id_r : kind_t Identifier.t option ref = ref None

    let mark_reduce_r : bool ref = ref false

    let codebase_r : Codebase.t ref = ref Codebase.empty

    let dependencies_with_path_r : (dependency * scope) list option ref =
      ref None

    let local_deps_r : (dependency * scope) list option ref = ref None

    let validate ((fun_lib, fun_id)) =
      begin match fun_lib with
        | Some lib_name
          when not (is_ident lib_name) ->
          invalid_arg "Name of function must be a valid identifier!"
        | _ -> ()
      end

    (** Helper Functions **)

    (* This function shifts a location by an offset defined as the start of
       another location *)
    let shift_loc (l:Location.t) (by:Location.t) =
      let offset_x = (by.loc_start.pos_cnum) in
      let offset_y = by.loc_start.pos_lnum in
      let line = l.loc_start.pos_lnum - offset_y + 1 in
      let line' = l.loc_end.pos_lnum - offset_y + 1 in
      let column = l.loc_start.pos_cnum - offset_x in
      let column' = l.loc_end.pos_cnum - offset_x in
      (* Only the columns of the first line need to be shifted: *)
      let new_start =
        if line = 1 then
          {l.loc_start with pos_cnum = column ;
                            pos_lnum = line;
                            pos_bol = 0}
        else
          {l.loc_start with pos_lnum = line;}
      in
      let new_end =
        if line = 1 then
          {l.loc_end with pos_cnum = column' ;
                          pos_lnum = line;
                          pos_bol = 0}
        else
          {l.loc_end with pos_lnum = line'} in
      {l with loc_start = new_start; loc_end = new_end}

    (* Since we cannot use a set, we define a diff function over lists: *)
    let diff l1 l2 =
      List.filter (fun x ->
          not (List.mem ~eq:(fun x y -> Ident.same x.id y.id) x l2)) l1

    (** Visitors **)

    module IdentPathSet =
    struct
      include Set.Make(struct type t = Ident.t * Path.t
          let compare (x,y) (z,k) =
            match Ident.compare x z with
            | 0 -> Path.compare y k
            | n -> n
        end)
      class monoid = object
        inherit [t] VisitorsRuntime.monoid
        method private zero = empty
        method private plus = union
      end
      let of_opt =
        function
        | None ->
          empty
        | Some v ->
          singleton v
    end

    class ['self] result_reducer_path = object (self : 'self)
      inherit ['self] Typedtree_visitors.reduce
      inherit IdentPathSet.monoid
    end

    (* Visitor used to get constraints from module types:  *)
    let visitor_types env =
      object(self)
        inherit [_] result_reducer_path as super

        method! visit_ty_module_type path this =
          match this with
          | Mty_ident _ | Mty_alias _ ->
            let p = Types.get_module_type_path this in
            begin
              try
                let mtv = Moduletype.resolve env this in
                let Types_views.Ex mt = mtv in
                let s =  match mt with
                  | Types_views.ST s -> s
                  | Types_views.FT (_,_,_) -> failwith "Inlining does not work with functors"
                in
                self#visit_ty_signature path s
              with _ ->
                failwith (Format.sprintf "Cannot find %s in the environment" (Ast_lib.path_to_string p))
            end
          | Mty_signature _
          | Mty_functor (_,_,_) ->
            super#visit_ty_module_type path this

        method! visit_ty_signature_item path this =
          let open Types in 
          match this with
          | (Sig_value _) as sigv ->
            let id = get_sig_value_id sigv in
            IdentPathSet.singleton (id,path)
          | (Sig_module _ as sigm) ->
            let id = get_sig_module_id sigm in
            let path' = Path.make_pdot path (Ident.name id) in
            let r = super#visit_ty_signature_item path' this in
            self#plus (IdentPathSet.singleton (id,path)) r
          | (Sig_modtype _ as sigmt) ->
            let id = get_sig_modtype_id sigmt in
            let path' = Path.make_pdot path (Ident.name id) in
            let r = super#visit_ty_signature_item path' this in
            self#plus (IdentPathSet.singleton (id,path)) r
          | (Sig_type _ as sigt) ->
            let id = get_sig_type_id sigt in 
            IdentPathSet.singleton (id,path)
          | _ ->
            (* failwith "Something else" *)
            self#zero
      end

    (* This visitors gathers paths for all values and modules:
       e.g. for

       module A = struct
         let f x = x
         module B = struct
          let g x = x
        end
       end

       It computes the set :

       { (f,A) ; (B,A) ; (g,A.B) }

    *)
    let visitor_gather_pattern_paths =
      object(self)
        inherit [_] result_reducer_path as super

        (* MLI  *)

        method! visit_tt_value_description path this =
          IdentPathSet.singleton (this.val_id,path)

        method! visit_tt_module_declaration path this =
          let path' = Path.make_pdot path (Ident.name this.md_id) in
          let r = super#visit_tt_module_declaration path' this in
          self#plus (IdentPathSet.singleton (this.md_id,path)) r

        method! visit_tt_module_type_declaration path this =
          let path' = Path.make_pdot path (Ident.name this.mtd_id) in
          let r = super#visit_tt_module_type_declaration path' this in
          self#plus (IdentPathSet.singleton (this.mtd_id,path)) r

        method! visit_tt_module_type path this =
          let d = (match this.mty_desc with
              | Tmty_ident (p,lid) ->
                (* Get the module type definition  *)
                let mtv = Moduletype.resolve this.mty_env this.mty_type in
                let Types_views.Ex mt = mtv in
                let s =  match mt with
                  | Types_views.ST s -> s
                  | Types_views.FT _ -> failwith "Inlining does not work with functors"
                in
                (* Visit the module type signature *)
                (visitor_types this.mty_env)#visit_ty_signature path s
              | Tmty_signature s ->
                (visitor_types this.mty_env)#visit_ty_signature path s.sig_type
              | _ -> self#zero) in
          let r = super#visit_tt_module_type path this in
          self#plus d r

        (* ML *)

        method! visit_tt_value_binding path this =
          match this.vb_pat.pat_desc with
          | Tpat_var (id, stringloc) ->
            (* No need to go inside the value definition (we only gather
               top-level definitions) *)
            IdentPathSet.singleton (id,path)
          | _ ->
            super#visit_tt_value_binding path this

        method! visit_tt_module_binding path this =
          let path' = Path.make_pdot path (Ident.name this.mb_id) in
          let r = super#visit_tt_module_binding path' this in
          self#plus (IdentPathSet.singleton (this.mb_id,path)) r
      end



    class ['self] result_reducer_ident = object (self : 'self)
      inherit ['self] Typedtree_visitors.reduce
      method private zero = []
      method private plus = (@)
    end

    (* This visitor returns all variables it can find
       (except locally defined variables).
       It is used to gather every dependency of a function: *)
    let visitor_gather_dependencies(source,env) =
      object(self)
        inherit [_] result_reducer_ident as super

        val mutable last_id = None

        method! visit_pt_pattern p this =
          match this.ppat_desc with
          | Ppat_var sloc ->
            begin match last_id with
              | None -> []
              | Some last_id -> 
                [{ id = last_id;
                   path = Pident last_id ;
                   lid = Longident.build [sloc.txt] ;
                   loc = sloc.loc }]
            end
          | _ -> super#visit_pt_pattern () this

        method! visit_tt_pattern p this =
          match this.pat_desc with
          | Tpat_var (id,sloc) ->
            [{ id = id;
               path = Pident id ;
               lid = Longident.build [sloc.txt] ;
               loc = sloc.loc }]
          | _ -> super#visit_tt_pattern () this

        method! visit_tt_expression _ this =
          match this.exp_desc with
          | Texp_ident (p, lid, _) ->
            (* TODO: normalize path ? *)
            let env =
              find_map
                (fun (x,_,_) -> env_of_exp_extra x)
                (List.rev this.exp_extra) in
            let env = Option.get_or ~default:this.exp_env env in
            let _norm = Env.normalize_path_prefix (Some lid.loc) env p in
            (* Format.printf "Path after %a\n" Path.print norm; *)
            let id = Path.head p in
            [{ id = id ; path = p ; lid = lid.txt ; loc = lid.loc}]
          | Texp_let (_,vblist,e) ->
            let s = super#visit_tt_expression () this in
            let bounded = List.fold_left
                (fun acc vb -> self#plus
                    (self#visit_tt_pattern () vb.vb_pat) acc)
                self#zero vblist in
            (* Remove variables introduced by the let binding *)
            diff s bounded
          | Texp_for (id,p,e1,e2,dir,e) ->
            last_id <- Some id ;
            let s = super#visit_tt_expression () this in
            let bounded = self#visit_pt_pattern () p in
            (* Remove variables introduced by the for loop *)
            diff s bounded
          | (Texp_match _) as texp ->
            let cases = get_match_cases texp in 
            let s = super#visit_tt_expression () this in
            let pats = List.fold_left
                (fun acc c -> self#plus
                    (self#visit_tt_pattern () Typedtree.(c.c_lhs)) acc)
                self#zero cases in
            (* Remove variables bound by pattern matching *)
            diff s pats
          | Texp_function { cases ; _ } ->
            let s = super#visit_tt_expression () this in
            let pats = List.fold_left
                (fun acc c -> self#plus
                    (self#visit_tt_pattern () Typedtree.(c.c_lhs)) acc)
                self#zero cases in
            (* Remove arguments of the local function *)
            diff s pats
          | _ -> super#visit_tt_expression () this
      end



    open Refactoring_visitors

    class ['a] env id scope e = object (self)
      inherit ['a] id_env id
      inherit module_scope_env scope
      val _env : Env.t = e
      method env = _env
    end

    open Ast_utils

    let constraints = ref None

    let check_deps_against_constraints deps =

      match !constraints with
      | None -> ()
      | Some constraints ->
        let constraints = List.map (fun (x,s) ->
            let s' = Path.to_longident s in
            let s' = Ast_lib.longident_to_string s' in
            String.concat "." [s';Ident.name x]) constraints in
        let check_one_dep (x,scope) =
          let lid = Path.to_longident x.path in
          let fullname = Ast_lib.longident_to_string lid in
          let s' = Path.to_longident scope in
          let s' = Ast_lib.longident_to_string s' in
          let full_path = String.concat "." [s';fullname] in
          if
            List.mem ~eq: String.equal full_path constraints
          then () else
            failwith (Format.sprintf "The dependency %s is not exported by the \
                                      corresponding module signature" full_path)
        in
        List.iter check_one_dep deps


    (* Remove all non-local dependencies  *)
    let keep_local_deps ds modid =
      let modids = Longident.flatten modid in
      let aux (d,p) =
        let id = Path.to_longident p in
        let ids = Longident.flatten id in
        let rec loop modids ids =
          match modids, ids with
          | [] , [] -> true
          | [] , _ -> true
          | x::xs , y::ys when String.equal x y ->
            loop xs ys
          | _ -> false
        in
        loop modids ids
      in
      List.filter aux ds

    (* This visitor goes through an AST looking for a module type (and returns it) *)
    let visitor_mtypes =
      object(self)
        inherit [_] reduce_with_id as super
        method private zero = None
        method private plus x y =
          match x, y with
          | None, None -> None
          | None, Some y -> Some y
          | Some x, None -> Some x
          | Some x, Some y -> Some y

        method! visit_tt_module_binding env mb =
          let id = Chain.tl_exn env#id in
          let scope, _ =
            Modulescope.enter_module_binding (InStr mb) env#scope in
          super#visit_tt_module_binding (new env id scope env#env) mb

        method! visit_tt_include_declaration env decl =
          let scope, _ = Modulescope.enter_include (InStr decl) env#scope in
          super#visit_tt_include_declaration (env#with_scope scope) decl

        method! visit_tt_module_declaration env md =
          let id = Chain.tl_exn env#id in
          let scope, _ =
            Modulescope.enter_module_binding (InSig md) env#scope in
          super#visit_tt_module_declaration (new env id scope env#env) md

        method! visit_tt_module_type_declaration env mtd =
          mtd.mtd_type

        method! process_binding_from_struct env this
            (type a)
            ((id : (Ident.t, a) Atom.t), _)
            (((_, _, _, binding) as _source) : (a, impl) binding_source)=

          let locals =
            match id, binding with
            | _, `Binding Typedtree_views.Value (_, Typedtree_views.InStrVal bv) ->
              self# visit_tt_value_binding env bv
            | _, `Binding Typedtree_views.Structure (_, Typedtree_views.InStr mb) ->
              self#visit_tt_module_binding env mb
            | _, `Binding Typedtree_views.StructureType Typedtree_views.InStr mtd ->
              self#visit_tt_module_type_declaration env mtd
            | _ -> None
          in
          locals
      end

    (* This visitor looks for the function to inline, and gets its body and
       its dependencies: *)
    let visitor(name,source,paths) =
      object(self)

        inherit [_] reduce_with_id as super
        method private zero = ()
        method private plus x y = ()

        method! visit_tt_structure_item_desc scope this =
          match this with
          | Tstr_eval _
          | Tstr_value _
          | Tstr_primitive _
          | Tstr_type _
          | Tstr_typext _
          | Tstr_exception _
          | Tstr_open _
          | Tstr_class _
          | Tstr_class_type _
          | Tstr_attribute _
            ->
            self#zero
          | Tstr_module _
          | Tstr_recmodule _
          | Tstr_modtype _
          | Tstr_include _
            ->
            super#visit_tt_structure_item_desc scope this

        method! process_binding_from_struct env this
            (type a)
            ((id : (Ident.t, a) Atom.t), _)
            (((_, _, _, binding) as _source) : (a, impl) binding_source)=

          let locals =
            match id, binding with
            | _, `Binding Typedtree_views.Value (_, Typedtree_views.InStrVal bv) ->
              self# visit_tt_value_binding env bv
            | _, `Binding Typedtree_views.Structure (_, Typedtree_views.InStr mb) ->
              self#visit_tt_module_binding env mb
            | _, `Binding Typedtree_views.StructureType Typedtree_views.InStr mtd ->
              self#visit_tt_module_type_declaration env mtd
            | _, `Include Typedtree_views.InStr decl ->
              self#visit_tt_include_declaration env decl
            | _ -> failwith "Locals"

          in
          locals

        method! visit_tt_value_binding env ({vb_pat ; vb_expr ; _} as this) =
          match vb_pat.pat_desc with
          | Tpat_var (id,sloc) ->
            let scope = env#scope in
            (* Get all dependencies of the function to inline: *)
            let ldeps = (visitor_gather_dependencies (source,env))#visit_tt_expression ()
                vb_expr in
            (* Get the body of the function (as a source code): *)
            (* Known issue: The inlined code may have a weird indentation
               because it is copy/pasted from where it is defined *)
            let body_str = Sourcefile.extract_location source vb_expr.exp_loc in
            (* Every dependency is replaced by its full name (i.e. "open" are
               resolved) + by its unique identifier (e.g. x_80) *)
            let repls = List.fold_left (fun acc (d:dependency) ->
                let new_loc = shift_loc d.loc vb_expr.exp_loc in
                let s = Ident.unique_name d.id in
                (* We must remove Stdlib/Pervasives from the dependencies
                   because Stdlib.+ gives a parse error ...
                   Issue : if + (or -,print_string, ...) is redefined
                   in the file where the function is inlined, then the inlined
                   code is not equivalent to the function call
                   Possible fix : add a let open Stdlib in the beginning of
                   the inlined code
                   Other related issue: if the function uses a redefinition of
                   an infix operator, the inlined code will be wrong (e.g Z.+)
                *)
                let drop_stdlib p =
                  let lid = Path.to_longident p in
                  let ids = Longident.flatten lid in
                  match ids with
                  | "Stdlib" :: ids -> Longident.build ids
                  | "Pervasives" :: ids -> Longident.build ids
                  | _ -> lid
                in
                let lid = drop_stdlib d.path in
                let fullname = Ast_lib.longident_to_string lid in
                let payload =
                  if Ident.global d.id then fullname else
                    String.replace
                      ~which:`Left ~sub:(Ident.name d.id) ~by:s fullname in
                Replacement.Set.add (Replacement.mk new_loc payload) acc
              ) Replacement.Set.empty ldeps in
            let body_str = Replacement.apply_all repls body_str in
            (* Transform the body into a lambda-expression: *)
            let body_str =
              match vb_expr.exp_desc with
              | Texp_function _ ->
                let funbody = String.replace
                    ~which:`Left ~sub:"=" ~by:"->" body_str in
                if !mark_reduce_r then
                  String.replace ~sub:"_f" ~by:funbody ("((fun _f)[@rotor_reduce])")
                else
                  String.replace ~sub:"_f" ~by:funbody ("(fun _f)")

              | _ -> String.replace ~sub:"_f" ~by:body_str ("(_f)")
            in
            (* Save pairs (dependency*path) for later *)
            let ldeps_with_path = List.fold_left (fun acc d ->
                try
                  let scope = List.assoc ~eq:Ident.same d.id paths in
                  (d,scope)::acc
                with Not_found ->
                  (* This happens when dependencies come from Stdlib *)
                  acc
              ) [] ldeps in

            (* Check that none of the dependencies are shadowed by definitions that happen after the function  *)
            let check_dep_shadowing (d,p) =
              let test = List.fold_left (fun acc (x,p) -> if String.equal (Ident.name d.id) (Ident.name x) then Some (x,p) else acc  ) None paths in
              match test with
              | None -> failwith (Format.sprintf "Cannot find %a" Ident.print d.id)
              | Some (a,b) -> if Path.same p b && not (Ident.same d.id a) then
                  failwith (Format.sprintf "Cannot inline: dependency %s.%s is shadowed by another definition in the same module" (Ast_lib.path_to_string p) (Ident.name d.id))
                else
                  ()
            in
            List.iter check_dep_shadowing ldeps_with_path;

            dependencies_with_path_r := Some ldeps_with_path ;
            let id = Modulescope.to_identifier scope in
            let id = Option.get_exn id in
            let Chain.Ex (_,id) = id in
            let id = Chain.to_longident id in
            let local_deps = keep_local_deps ldeps_with_path id in
            local_deps_r := Some local_deps;
            if Option.is_none !fun_body_r then
              fun_body_r := Some body_str;
          | _ ->
            super#visit_tt_value_binding env this

        method! visit_tt_module_binding env mb =
          let id = Chain.tl_exn env#id in
          let scope, _ =
            Modulescope.enter_module_binding (InStr mb) env#scope in
          super#visit_tt_module_binding (new env id scope env#env) mb

        method! visit_tt_include_declaration env decl =
          let scope, _ = Modulescope.enter_include (InStr decl) env#scope in
          super#visit_tt_include_declaration (env#with_scope scope) decl

        method! visit_tt_module_declaration env md =
          let id = Chain.tl_exn env#id in
          let scope, _ =
            Modulescope.enter_module_binding (InSig md) env#scope in
          super#visit_tt_module_declaration (new env id scope env#env) md

        method! visit_tt_module_type_declaration env mtd =
          let id = Chain.tl_exn env#id in
          let scope, _ =
            Modulescope.enter_module_type_declaration mtd env#scope in
          super#visit_tt_module_type_declaration (new env id scope env#env) mtd

        (* Do not go further into signature items : *)
        method! visit_tt_signature_item env this = self#zero

        method! visit_ty_signature env this =
          (* Get current path  *)
          let p = Modulescope.to_identifier env#scope in
          let p = Option.get_exn p in
          let Chain.Ex (_,c) = p in
          let lid = Chain.to_longident c in
          let sid = Ast_lib.longident_to_string lid in
          let p = Path.Pident (Ident.create_persistent sid) in
          let ids = (visitor_types env#env)#visit_ty_signature p
              this in
          constraints :=
            begin match IdentPathSet.to_list ids with
              | [] -> None
              | l -> Some l
            end

        method! visit_tt_module_type env this =
          (* Save environment of the module type  *)
          let env = (new env env#id env#scope this.mty_env) in
          (match this.mty_desc with
           | Tmty_ident (p,lid) ->
             (* Get the module type definition  *)
             let mtv = Moduletype.resolve this.mty_env this.mty_type in
             let Types_views.Ex mt = mtv in
             let s =  match mt with
               | Types_views.ST s -> s
               | Types_views.FT _ -> failwith "Inlining does not work with functors"
             in
             (* Visit the module type signature *)
             self#visit_ty_signature env s ;
           | Tmty_signature s ->
             self#visit_ty_signature env s.sig_type
           | _ -> ());
          super#visit_tt_module_type env this

      end

    (*** Getters ***)

    let fun_lib () =
      Option.flat_map fst !fun_id_r

    let fun_id () = match !fun_id_r with
      | Some (_, id) -> id
      | None ->
        let err = Not_initialised "Function to inline not specified!" in
        raise (Error (err, None))

    let mark_reduce () = !mark_reduce_r

    let fun_body () = match !fun_body_r with
      | Some s -> s
      | None ->
        let err = Not_initialised "Body of function unknown!" in
        raise (Error (err, None))

    let codebase () =  !codebase_r

    let dependencies_with_path () =
      match !dependencies_with_path_r with
      | Some s -> s
      | None ->
        let err = Not_initialised "Dependencies of function unknown!" in
        raise (Error (err, None))

    (*** Ancillary ***)

    let is_local input =
      let open Fileinfos in
      let libs_match =
        match input.library, (fun_lib ()) with
        | None, None -> true
        | Some lib, Some lib' ->
          String.equal
            (String.capitalize_ascii lib)
            (String.capitalize_ascii lib')
        | _ -> false in
      let modules_match =
        let open Chain in
        match fun_id () with
        | InStructure (m, _) ->
          String.equal m (module_name input)
        | _ -> false in
      libs_match && modules_match

    (*** Implementations for Refactoring Interface ***)

    let to_repr () =
      Refactoring_repr.Inline
        ((fun_lib (), Chain.Ex (kind, fun_id ())),mark_reduce (), codebase ())

    let kernel_mem f = is_local f

    let kernel c =
      let c = Codebase.filter is_local c in
      if Codebase.is_empty c then
        let err =
          KernelError
            (Format.sprintf "kernel for %a is empty"
               Refactoring_repr.pp (to_repr ())) in
        raise (Error (err, None))
      else
        Codebase.to_list c

    (* Check if a dependency appears in a signature list *)
    let check_sigs sigs (deps: (dependency * scope) list) =
      let sigs = List.map (fun (x,s) ->
          let s' = Path.to_longident s in
          let s' = Ast_lib.longident_to_string s' in
          String.concat "." [s';Ident.name x]) sigs in
      let check_one_dep (x,scope) =
        let lid = Path.to_longident x.path in
        let fullname = Ast_lib.longident_to_string lid in
        let s' = Path.to_longident scope in
        let s' = Ast_lib.longident_to_string s' in
        let full_path = String.concat "." [s';fullname] in
        if
          List.mem ~eq: String.equal full_path sigs
        then () else
          failwith (Format.sprintf "The dependency %s is not exported by the co\
                                    rresponding mli" full_path)
      in
      List.iter check_one_dep deps

    (* Check that every dependency of the inlined function is in the mli *)
    let check_mli implem codebase fileid =
      let mli_name = Lib.get_mli (Fileinfos.filename implem) in
      let mli = Codebase.find_file mli_name codebase in
      let () = Option.iter (fun mli ->
          let mli_source = Sourcefile.of_fileinfos mli in
          let mli_paths =
            match mli_source.ast with
            | Interface (pt,Some tt) ->
              let v = visitor_gather_pattern_paths in
              v#visit_tt_signature (Pident fileid) tt
            | _ -> assert false
          in
          let mli_paths = IdentPathSet.to_list mli_paths in
          check_sigs mli_paths (dependencies_with_path ())

        ) mli in
      ()

    (*** Initialisation ***)

    (* Initialisation of the refactoring needs to visit the definition of
       the function to inline: *)
    let init (funid,mark_reduce,codebase) =

      validate funid;
      fun_id_r := Some funid;
      mark_reduce_r := mark_reduce;
      codebase_r := codebase;
      let name = let Atom.Data.{ id; _ } =
                   Atom.unwrap (Chain.anchor (snd funid)) in id in
      let kern = kernel codebase in
      let implem = List.find (fun f ->
          let ext = Filename.extension (Fileinfos.filename f) in
          String.equal ".ml" ext) kern in
      let filename = Fileinfos.module_name implem in
      let fileid = Ident.create_persistent filename in
      let source = Sourcefile.of_fileinfos implem in
      (* Get pattern of all functions in the file  *)
      let all_paths = match source.ast with
        | Implementation (pt,Some tt) ->
          let v = visitor_gather_pattern_paths in
          v#visit_tt_structure
            (Pident fileid) tt
        | _ -> assert false
      in
      let paths = IdentPathSet.to_list all_paths in
      let scope = ((Fileinfos.module_name implem, None), []) in
      let from_id = fun_id () in
      let from_id = Chain.tl_exn (from_id) in
      let v = visitor(name,source,paths) in
      (* Get the source of the inlined function and its dependencies *)
      let () = match source.ast with
        | Implementation (pt,Some tt) ->
          v#visit_tt_structure
            (new env from_id scope Env.empty) tt
        | _ -> assert false
      in
      (* TODO : add a command line option to trigger this or not  *)
      (match !dependencies_with_path_r with
       | None -> dependencies_with_path_r := Some []
       | _ -> ());
      check_mli implem codebase fileid;
      (match !fun_body_r with
       | None ->
         let s = Format.sprintf "File %s : could not find the body of the function to inline" filename in
         failwith  s
       | _ -> ());
      Option.iter( fun x ->
          check_deps_against_constraints x) !local_deps_r

    let initialise args =
      if List.length args < 1 then
        invalid_arg "Not enough parameters!"
      else
        let (fun_lib, fun_id) = of_string (List.nth args 0) in
        let fun_id =
          try
            Chain.try_open X.kind fun_id
          with KindMismatch msg ->
            invalid_arg
              (Format.sprintf
                 "Invalid identifier for extension: %s" msg) in
        let codebase = Codebase.empty in
        ignore (failwith "TODO");
        init ((fun_lib, fun_id),false,codebase)
  end

end
