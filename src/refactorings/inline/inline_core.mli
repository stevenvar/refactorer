module ParameterState : sig
  type dependency = { id : Ident.t; 
                      path : Path.t ;
                      lid : Longident.t;
                      loc : Location.t }

  type scope = Path.t

  module type S = sig
    type kind_t
    val kind : kind_t Elements.Base.t
    val initialise : string list -> unit
    val init : kind_t Identifier.t * bool * Codebase.t -> unit
    val fun_lib : unit -> string option
    val fun_id : unit -> kind_t Identifier.Chain.t
    val dependencies_with_path : unit -> ( dependency * scope ) list
    val mark_reduce : unit -> bool
    val codebase : unit -> Codebase.t
    val fun_body : unit -> string
    val to_repr : unit -> Refactoring_repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make
      (X : sig
         type kind_t
         val kind : kind_t Elements.Base.t
       end)
    : S with type kind_t = X.kind_t
end
