open Containers

(* Exceptions *)

(* Errors indicate the irrecoverable failure of a refactoring.
   We follow the pattern found in the compiler of defining a datatype to
   encapsulate the various different kinds of failures that can occur, with
   values wrapped by an Error exception. The module then also provides
   functions for formatting and logging an error.
*)

type t =
  | Not_initialised of string
    (** [Not_initialised msg] indicates that the refactoring has not
        been properly initialised; [msg] provides an error message. *)
  | Bad_sourcefile of string
    (** [Bad_sourcefile msd] indicates that the representation of the source
        code was insufficient to requirements; [msg] provides an error message. *)
  | CompilerError of Location.error
    (** [CompilerError err] indicates that an error was raised by a function in
        the compiler that was used whilst analysis the source representation;
        [err] is the error in question. *)
  | SoundnessViolation of string
    (** [SoundnessViolation msg] indicates that apply the refactoring is
        unsound; [msg] provides a detailed error message. *)
  | KernelError of string
    (** [KernelError msg] indicates that a problem has been encountered during
        the computation of the kernel of a refactoring for a codebase;
        [msg] provides a detailed error message. *)


exception Error of t * Printexc.raw_backtrace option
(** An Error indicates the irrecoverable failure of a refactoring. *)

val print_error : t Format.printer
(** [report_error fmt e] reports the error [e] on the formatter [fmt]. *)
val log_error : ?tags:Logs.Tag.set -> t -> unit
(** [log_error e] logs the error on [Logging.log]. *)