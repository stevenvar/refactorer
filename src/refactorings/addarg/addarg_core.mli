module ParameterState : sig

  module type S = sig
    type kind_t
    val kind : kind_t Elements.Base.t
    val initialise : string list -> unit
    val init : kind_t Identifier.t * string * string * string -> unit
    val fun_lib : unit -> string option
    val fun_id : unit -> kind_t Identifier.Chain.t
    val arg_name : unit -> string
    val typ : unit -> string
    val default_value : unit -> string
    val to_repr : unit -> Refactoring_repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make
      (X : sig
         type kind_t
         val kind : kind_t Elements.Base.t
       end)
    : S with type kind_t = X.kind_t
end