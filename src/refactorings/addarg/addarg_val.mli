open Refactoring_sigs

module Make() :
sig
  include Refactoring
  val init : Elements.Base._value Identifier.t * string * string *  string -> unit
end