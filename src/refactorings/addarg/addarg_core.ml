open Containers

open Elements
open Elements.Base

open Identifier
open Refactoring_error

module ParameterState = struct

  module type S = sig
    type kind_t
    val kind : kind_t Base.t
    val initialise : string list -> unit
    val init : kind_t Identifier.t * string * string * string -> unit
    val fun_lib : unit -> string option
    val fun_id : unit -> kind_t Chain.t
    val arg_name : unit -> string
    val typ : unit -> string
    val default_value : unit -> string
    val to_repr : unit -> Refactoring_repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make
      (X : sig
         type kind_t
         val kind : kind_t Base.t
       end)
    : S with type kind_t = X.kind_t =
  struct

    type kind_t = X.kind_t

    let kind = X.kind

    let fun_id : kind_t Identifier.t option ref = ref None
    let arg_name : string option ref = ref None
    let typ : string option ref = ref None
    let default_value : string option ref = ref None

    let validate ((fun_lib, fun_id), arg_name, typ, default_value) =
      begin match fun_lib with
        | Some lib_name
          when not (is_ident lib_name) ->
          invalid_arg "Name of function must be a valid identifier!"
        | _ -> ()
      end ;
      begin if not (is_lowercase_ident arg_name) then
          invalid_arg "Name of argument must be a valid identifier!"
      end

    let init ((id, arg, ty, default) as params) =
      validate params ;
      fun_id := Some id ;
      arg_name := Some arg;
      typ := Some ty;
      default_value := Some default

    let initialise args =
      if List.length args < 4 then
        invalid_arg "Not enough parameters!"
      else
        let (fun_lib, fun_id) = of_string (List.nth args 0) in
        let fun_id =
          try
            Chain.try_open X.kind fun_id
          with KindMismatch msg ->
            invalid_arg
              (Format.sprintf
                 "Invalid identifier for extension: %s" msg) in
        let arg = List.nth args 1 in
        let typ = List.nth args 2 in
        let default = List.nth args 3 in
        init ((fun_lib, fun_id), arg,typ,default)

    (*** Getters ***)

    let fun_lib () =
      Option.flat_map fst !fun_id

    let fun_id () = match !fun_id with
      | Some (_, id) -> id
      | None ->
        let err = Not_initialised "Identifier to extend not specified!" in
        raise (Error (err, None))

    let arg_name () = match !arg_name with
      | Some id -> id
      | None ->
        let err = Not_initialised "Name of new argument not specified!" in
        raise (Error (err, None))

    let typ () = match !typ with
      | Some id -> id
      | None ->
        let err = Not_initialised "Name of new argument not specified!" in
        raise (Error (err, None))

    let default_value () = match !default_value with
      | Some id -> id
      | None ->
        let err = Not_initialised "Name of new argument not specified!" in
        raise (Error (err, None))

    (*** Ancillary ***)

    let is_local input =
      let open Fileinfos in
      let libs_match =
        match input.library, (fun_lib ()) with
        | None, None -> true
        | Some lib, Some lib' ->
          String.equal
            (String.capitalize_ascii lib)
            (String.capitalize_ascii lib')
        | _ -> false in
      let modules_match =
        let open Chain in
        match fun_id () with
        | InStructure (m, _) ->
          String.equal m (module_name input)
        | _ -> false in
      libs_match && modules_match

    (*** Implementations for Refactoring Interface ***)

    let to_repr () =
      Refactoring_repr.Addarg ((fun_lib (), Chain.Ex (kind, fun_id ())), arg_name (), typ (), default_value ())

    let kernel_mem f = is_local f

    let kernel c =
      let c = Codebase.filter is_local c in
      if Codebase.is_empty c then
        let err =
          KernelError
            (Format.sprintf "kernel for %a is empty" Refactoring_repr.pp (to_repr ())) in
        raise (Error (err, None))
      else
        Codebase.to_list c
        (* TODO: Find a more efficient way of doing this *)

  end

end