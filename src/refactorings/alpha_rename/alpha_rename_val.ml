open Containers

open Elements
open Elements.Base

open Identifier
open Refactoring_error

open Refactoring_visitors
open Refactoring_utils

open   Typedtree
open Ast_visitors

module ParameterState = struct

  module type S = sig
    type kind_t
    val kind : kind_t Base.t
    val initialise : string list -> unit
    val init : kind_t Identifier.t * int * int * string -> unit
    val fun_lib : unit -> string option
    val fun_id : unit -> kind_t Chain.t
    val lnum : unit -> int
    val cnum : unit -> int
    val new_name : unit -> string
    val to_repr : unit -> Refactoring_repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make  (X : sig
      type kind_t
      val kind : kind_t Base.t
    end)
    : S with type kind_t = X.kind_t = struct

    type kind_t = X.kind_t

    let kind = X.kind

    let fun_id : kind_t Identifier.t option ref = ref None
    let lnum : int option ref = ref None
    let cnum : int option ref = ref None
    let new_name : string option ref = ref None

    let init (id, _lnum, _cnum, _new_name) =
      fun_id := Some id;
      lnum := Some _lnum;
      cnum := Some _cnum;
      new_name := Some _new_name

    let fun_lib () =
      Option.flat_map fst !fun_id

    let fun_id () =
      match !fun_id with
      | Some (_, id) -> id
      | None ->
        let err = Not_initialised "Identifier of file not specified!" in
        raise (Error (err, None))

    let lnum () = match !lnum with
      | Some num -> num
      | None ->
        let err = Not_initialised "Line number not specified!" in
        raise (Error (err, None))

    let cnum () = match !cnum with
      | Some num -> num
      | None ->
        let err = Not_initialised "Column number not specified!" in
        raise (Error (err, None))

    let new_name () = match !new_name with
      | Some name -> name
      | None ->
        let err = Not_initialised "New name not specified!" in
        raise (Error (err, None))

    let initialise args =
      if List.length args < 4 then
        invalid_arg "Not enough parameters!"
      else
        let (module_lib, module_id) = Identifier.of_string (List.nth args 0) in
        let module_id =
          try
            Identifier.Chain.try_open X.kind module_id
          with KindMismatch msg ->
            invalid_arg
              (Format.sprintf
                 "Invalid identifier for extension: %s" msg) in
        let lnum = int_of_string (List.nth args 1) in
        let cnum = int_of_string (List.nth args 2) in
        let new_name = List.nth args 3 in
        init ((module_lib, module_id), lnum, cnum, new_name)

    let to_repr () =
      Refactoring_repr.AlphaRename
        ((fun_lib (), Chain.Ex (kind, fun_id ())), lnum (), cnum (), new_name ())

    let is_local input =
      let open Fileinfos in
      let libs_match =
        match input.library, (fun_lib ()) with
        | None, None -> true
        | Some lib, Some lib' ->
          String.equal
            (String.capitalize_ascii lib)
            (String.capitalize_ascii lib')
        | _ -> false in
      let modules_match =
        let open Chain in
        match fun_id () with
        (* | Atomic (Structure s) ->
          String.equal s (module_name input) *)
        | _ ->
          false in
      libs_match && modules_match

    let kernel c =
      let c = Codebase.filter is_local c in
      if Codebase.is_empty c then
        let err =
          KernelError
            (Format.sprintf "kernel for %a is empty"
               Refactoring_repr.pp (to_repr ())) in
        raise (Error (err, None))
      else
        Codebase.to_list c

    let kernel_mem f = is_local f

  end
end

module Make () = struct

  module InState = InputState.Make ()
  module Param = ParameterState.Make (struct
      type kind_t = _structure
      let kind = Elements.Base.Structure
    end)
  include Param

  let name = "alpha-rename"

  let get_deps ~mli source = Refactoring_deps.Elt.Set.empty

  module Reps = struct

    let is_in_interval (cnum,lnum) pos =
      let open Location in
      let loc_end = pos.loc_end in
      let loc_start = pos.loc_start in
      let line = loc_start.pos_lnum in
      let line_end = loc_end.pos_lnum in
      let col_end = loc_end.pos_cnum - loc_end.pos_bol in
      let col = loc_start.pos_cnum - loc_start.pos_bol in
      cnum >= col && cnum <= col_end
      && lnum >= line && lnum <= line_end

    module IdentOption =
    struct
      class monoid = object
        method private zero = None
        method private plus x y =
          match x,y with
          | None, _ -> y
          | _, None -> x
          | Some x, Some y -> failwith "Clash"
      end

    end

    class ['self] result_reducer_id = object (self : 'self)
      inherit ['self] Typedtree_visitors.reduce
      inherit IdentOption.monoid
    end

    module StringSet = Set.Make(String)


    let visitor_find_used_names = object (self)
      inherit ['self] Typedtree_visitors.reduce as super

      method zero = StringSet.empty
      method plus = StringSet.union

      method! visit_tt_pattern ctx this =
        match this.pat_desc with
        | Tpat_var (id,strloc) ->
          StringSet.singleton strloc.txt
        | _ -> super#visit_tt_pattern ctx this

      method! visit_tt_expression ctx this =
        match this.exp_desc with
        | Texp_ident (path,lidloc,_) ->
          let name = Ast_lib.longident_to_string lidloc.txt in
          StringSet.singleton name
        | _ -> super#visit_tt_expression ctx this

      method! visit_tt_structure_item ctx this =
        if is_in_interval (cnum (), lnum ()) this.str_loc then
          super#visit_tt_structure_item ctx this
        else
          self#zero

    end

    let visitor_find_id = object (self)

      inherit [_] result_reducer_id as super

      method! visit_tt_pattern ctx this =
        match this.pat_desc with
        | Tpat_var (id,strloc) ->
          if is_in_interval (cnum (), lnum ()) strloc.loc then
            Some id
          else None
        | _ -> super#visit_tt_pattern ctx this

      method! visit_tt_expression ctx this =
        match this.exp_desc with
        | Texp_ident (path,lidloc,_) ->
          if is_in_interval (cnum (), lnum ()) lidloc.loc then
            let ident = Path.head path in
            Some ident
          else None
        | _ -> super#visit_tt_expression ctx this

      method! visit_tt_structure_item ctx this =
        if is_in_interval (cnum (), lnum ()) this.str_loc then
          super#visit_tt_structure_item ctx this
        else
          self#zero

    end


    let visitor ident = object(self)

      inherit [_] result_reducer as super

      method! visit_tt_pattern ctx this =
        match this.pat_desc with
        | Tpat_var (id,strloc) ->
          if Ident.same id ident then
            Replacement.Set.singleton (Replacement.mk strloc.loc (new_name ()))
          else
            self#zero
        | _ -> super#visit_tt_pattern ctx this

      method! visit_tt_expression ctx this =
        match this.exp_desc with
        | Texp_ident (path,lidloc,_) ->
          let id = Path.head path in
          if Ident.same id ident then
            Replacement.Set.singleton (Replacement.mk this.exp_loc (new_name ()))
          else
            self#zero
        | _ -> super#visit_tt_expression ctx this

      method! visit_tt_structure_item ctx this =
        if is_in_interval (cnum (), lnum ()) this.str_loc then
          super#visit_tt_structure_item ctx this
        else
          self#zero

    end

  end

  let process_sig scope _sig =
    (* do nothing in mlis  *)
    Replacement.Set.empty

  let process_struct scope _struct =
    let id = Reps.(visitor_find_id#visit_tt_structure () _struct) in
    match id with
    | None -> failwith "Couldn't find an identifier to rename at this location"
    | Some id when String.equal (Ident.name id) (new_name ())  -> Replacement.Set.empty
    | Some id ->
      let names = Reps.visitor_find_used_names#visit_tt_structure () _struct in
      (if Reps.StringSet.mem (new_name ()) names then
         let s = Format.sprintf "The identifier %s is already used in this function, alpha-renaming might introduce incorrect variable capture" (new_name ()) in
         failwith s);
      let replacements =
        Reps.
          ((visitor id)#visit_tt_structure None _struct) in
      let () = if Replacement.Set.is_empty replacements then
          Logging.warn @@ fun _f -> _f
            "Could not find local value to extend in %s!"
            (InState.get_input ()).fileinfos.filename in
      replacements

  let process_file input =
    let open Sourcefile in
    let open Fileinfos in
    let infos = input.fileinfos in
    let mod_scope = ((module_name infos, None), []) in
    let () = InState.set_input input in
    InState.dispatch_on_input_type
      ~intf_f:(process_sig mod_scope)
      ~impl_f:(process_struct mod_scope)

end
