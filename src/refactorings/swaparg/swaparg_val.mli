open Refactoring_sigs

module Make() :
sig
  include Refactoring
  val init : Elements.Base._value Identifier.t * int * int -> unit
end