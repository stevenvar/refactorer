open Containers
open   List
open   Fun

open Compiler
open   Asttypes
open   Ident
open   Longident
open   Typedtree
open   Types
open   Compmisc

open Ast_visitors
open   Visitors_lib

open Refactoring_deps
open Refactoring_error
open Refactoring_repr
open Refactoring_utils
open Refactoring_visitors

open Lib
open Ast_lib
open Ast_utils
open Compiler_utils

open Elements.Base
open Identifier

open Typedtree_views

open Sourcefile
open Fileinfos

open Logging.Tags

open Swaparg_core

module DepSet = Refactoring_deps.Elt.Set

let rec retrieve_args node = match node.exp_desc with
  | Texp_function {cases = [{c_lhs = pat; c_guard = None; c_rhs = body}]; _} ->
    (pat.pat_loc) :: (retrieve_args body)
  | _ -> []

let rec retrieve_types node = match node.ctyp_desc with
  | Ttyp_arrow (_label, lhs, rhs) ->
    lhs.ctyp_loc :: (retrieve_types rhs)
  | _ -> []

let get_nth_type_loc n node =
  let rec aux k node =
    if k = 1 then
      match node.ctyp_desc with
      | Ttyp_arrow (_label, lhs, rhs) ->
        { lhs.ctyp_loc with loc_start = node.ctyp_loc.loc_start ;
                            loc_end = rhs.ctyp_loc.loc_start }
      | _ -> failwith "This is not a function"
    else
      match node.ctyp_desc with
      | Ttyp_arrow (_label, lhs, rhs) ->
        aux (k-1) rhs
      | _ -> failwith "not a function"
  in aux n node

module Make () = struct

  module InState = InputState.Make ()
  module Param = ParameterState.Make (struct
      type kind_t = _value
      let kind = Elements.Base.Value
    end)
  include Param

  (*** Properties ***)

  let name = "Swap arguments implementation value"


  let fun_name_as_str () =
    let Atom.Data.{ id; _ } =
      Atom.unwrap (Chain.anchor (fun_id ())) in
    id

  (* ------------------------------------------------------------------------
      REPLACEMENTS
     ------------------------------------------------------------------------- *)
  module Reps = struct

    module rec Nonlocal : sig
      include module type of struct
        let visitor
          : Ident.t * _value Chain.t ->
            < visit_tt_structure
              : [ `ARG_PUN | `FIELD_PUN | `NO_PUN ] -> _ ; .. > =
          fun _ ->
            object (self)
              inherit [_] result_reducer
            end
      end
    end = struct

      let shift_loc (l:Location.t) offset len =
        let new_start =
          {l.loc_start with pos_cnum = (l.loc_start.pos_cnum + offset - l.loc_start.pos_bol);
                            pos_lnum = 1;
                            pos_bol = 1} in
        let new_end = {l.loc_end with pos_cnum = min len (l.loc_end.pos_cnum + offset - l.loc_end.pos_bol);
                                      pos_lnum = 1;
                                      pos_bol = 1} in
        {l with loc_start = new_start; loc_end = new_end}

      let swapargs l n1 n2 =
        let t = Array.of_list l in
        let tmp = t.(n1) in
        t.(n1) <- t.(n2);
        t.(n2) <- tmp;
        Array.to_list t

      (* Check if name capture happens *)
      let check_soundness params args =
        List.fold_left
          (fun acc x -> List.mem String.equal x params || acc ) false args

      let rec create_arg_list id n =
        List.init n (fun i -> id^"_"^string_of_int (i+1)) 

      let visitor (ident, from_id) =
        object(self)

          inherit [_] result_reducer as super
          method! visit_tt_expression pun_tag
              ({ exp_env; exp_loc; exp_desc; exp_extra; _ } as this) =
            let env =
              find_map
                (fun (x,_,_) -> env_of_exp_extra x)
                (List.rev exp_extra) in
            let env = Option.get_or ~default:exp_env env in
            match exp_desc with
            (* Identifier of the function in case it is aliased or
               used as an argument for example *)
            | Texp_ident (p, { txt; loc; }, _) ->
              let p =
                try
                  Env.normalize_module_path (Some loc) env p
                with ((Env.Error e) as err) ->
                  let () = Logging.err @@ fun _f -> _f
                      "Error normalizing path %a: %a"
                      Printtyp.path p
                      Env.report_error e in
                  raise err
              in
              let head = Path.head p in
              let p =
                if Ident.persistent head then snd (Buildenv.Factorise.path p)
                else p in
              let id =
                Chain.of_path Elements.Base.Value p in
              if not ((Ident.same ident head) && Chain.equal id from_id) then
                self#zero
              else
                let str =
                  match pun_tag with
                  | `NO_PUN ->
                    let max_args = max (arg_num_one ()) (arg_num_two ()) in
                    let Atom.Data.{ id; _ } =
                      Atom.unwrap (Chain.anchor from_id) in
                    let params = create_arg_list id max_args in
                    let swapped_params =
                      swapargs params (arg_num_one ()-1) (arg_num_two ()-1) in
                    let lambda =
                      "(("^String.concat " "
                        (("fun"::params)@("->"::id::swapped_params))^")[@rotor_reduce])" in
                    String.replace ~which:`Right ~sub:id ~by:lambda
                      (longident_to_string txt)
                  | `ARG_PUN ->
                    failwith "Arg Pun"
                  | `FIELD_PUN ->
                    failwith "Field Pun"
                in
                Replacement.Set.of_opt (Replacement.mk_opt loc str)
            (* Applications of the function *)
            | Texp_apply ({ exp_desc = Texp_ident (p, { txt; loc; },_) },args) ->
              let p =
                try
                  Env.normalize_module_path (Some loc) env p
                with ((Env.Error e) as err) ->
                  let () = Logging.err @@ fun _f -> _f
                      "Error normalizing path %a: %a"
                      Printtyp.path p
                      Env.report_error e in
                  raise err
              in
              let head = Path.head p in
              let p =
                if Ident.persistent head then snd (Buildenv.Factorise.path p)
                else p in
              let id =
                Chain.of_path Elements.Base.Value p in
              if ((Ident.same ident head) && Chain.equal id from_id) then
                let repls =
                  match pun_tag with
                  | `NO_PUN ->
                    let max_args = max (arg_num_one ()) (arg_num_two ()) in
                    let Atom.Data.{id;_} = Atom.unwrap (Chain.anchor from_id) in
                    let params = create_arg_list id max_args in
                    let id_str = 
                      (longident_to_string txt) 
                    in
                    let swapped_params =
                      swapargs params (arg_num_one ()-1) (arg_num_two ()-1) in
                    let body = String.concat " " (id_str::swapped_params) in
                    let lambda =
                      "(("^String.concat " "
                        (("fun"::params)@["->";body])^")[@rotor_reduce])"
                    in
                    self#plus (Replacement.Set.singleton (Replacement.mk loc lambda))
                      (super#visit_tt_expression pun_tag this)

                  | `ARG_PUN ->
                    failwith "TODO: Punned arguments in application"
                  | `FIELD_PUN ->
                    failwith "TODO: Punned arguments in field"
                in
                repls
              else
                super#visit_tt_expression pun_tag this 
            | Texp_apply (_fun, args) ->
              (* Get the replacement set for the function part *)
              let result = self#visit_tt_expression pun_tag _fun in
              let process_arg prev_loc ((_, exp) as arg) =
                match exp.exp_desc with
                | Texp_ident _ when not exp.exp_loc.Location.loc_ghost ->
                  (* Set the correct pun tag and get replacement set for arg *)
                  begin try
                      let preamble = extract_src (InState.get_input ())
                          prev_loc
                          exp.exp_loc in
                      let pun_tag =
                        if is_arg_pun preamble arg then `ARG_PUN else `NO_PUN in
                      self#visit_tt_expression pun_tag exp
                    with Invalid_argument s ->
                      let () = Logging.warn @@ fun _f -> _f
                          "@[<v 4>%s@,%a@,%a@]" s
                          Location.print_loc prev_loc
                          Location.print_loc exp.exp_loc in
                      Replacement.Set.empty
                  end
                | _ -> self#visit_tt_expression pun_tag exp in
              (* Remove None args *)
              let args =
                List.filter_map
                  (fun (lbl, exp) -> (Option.map (Pair.make lbl) exp))
                  args in
              (* sort by location *)
              let args =
                List.sort
                  (fun (_, exp) (_, exp') -> cmp_loc exp.exp_loc exp'.exp_loc)
                  args in
              let result, args = match args with
                | [] ->
                  result, args
                | ((_, exp) as arg) :: args ->
                  (* only apply to args that come after the function *)
                  if cmp_loc exp.exp_loc _fun.exp_loc < 0 then
                    self#plus result (process_arg exp_loc arg), args
                  else
                    result, arg::args in
              (* Fold to a set of replacements  *)
              let result, _ =
                List.fold_left
                  (fun (result, prev_loc) ((_, exp) as arg) ->
                     (self#plus result (process_arg prev_loc arg)),
                     (if exp.exp_loc.Location.loc_ghost
                      then prev_loc else exp.exp_loc))
                  (result, _fun.exp_loc) args in
              result
            | Texp_record { fields; extended_expression; _ } ->
              let result = match extended_expression with
                | Some exp -> self#visit_tt_expression pun_tag exp
                | None -> Replacement.Set.empty in
              (* Check for punning when processing record fields
                 ----------------------------------------------------------------
                 The type checker fills in missing fields and rearranges them
                 to match the order they are given in the record definition. So
                 we need to filter out the ones not given in the source, and then
                 sort by location so that we can correctly compute each field's
                 preamble in the source text to check for punning. *)
              let process_fld prev_loc ((_, (_, exp)) as fld) =
                match exp.exp_desc with
                | Texp_ident _ when not exp.exp_loc.Location.loc_ghost ->
                  begin try
                      let preamble =
                        extract_src (InState.get_input ())
                          prev_loc exp.exp_loc in
                      let pun_tag =
                        if is_field_pun preamble fld
                        then `FIELD_PUN else `NO_PUN in
                      self#visit_tt_expression pun_tag exp
                    with Invalid_argument s ->
                      let () = Logging.warn @@ fun _f -> _f
                          "@[<v 4>%s@,%a@,%a@]" s
                          Location.print_loc prev_loc
                          Location.print_loc exp.exp_loc in
                      Replacement.Set.empty
                  end
                | _ -> self#visit_tt_expression pun_tag exp in
              let fields = Array.fold_right
                  (fun (lbl_desc, lbl_def) acc -> match lbl_def with
                     | Kept _ -> acc
                     | Overridden (loc, exp) -> (lbl_desc, (loc, exp))::acc)
                  fields [] in
              let fields = List.sort
                  (fun (_, (_, exp)) (_, (_, exp')) ->
                     cmp_loc exp.exp_loc exp'.exp_loc) fields in
              let result, _ = List.fold_left
                  (fun (result, prev_loc) ((_, (_, exp)) as lbl) ->
                     (self#plus result (process_fld prev_loc lbl)), exp.exp_loc)
                  (result,
                   (Option.map_or ~default:exp_loc (fun exp -> exp.exp_loc)
                      extended_expression))
                  fields in
              result
            | _ ->
              super#visit_tt_expression pun_tag this

          method! visit_tt_include_declaration env decl =
            let { incl_mod; incl_loc; incl_type; _ } = decl in
            begin match Module.unwind_expr incl_mod with
              | { mod_desc = Tmod_ident (p, _); _ } as me, apps, _ ->
                let p = Env.normalize_module_path (Some me.mod_loc) me.mod_env p
                in
                let head = Path.head p in
                let p =
                  if Ident.persistent (Path.head p)
                  then snd (Buildenv.Factorise.path p)
                  else p in
                if Ident.same ident head then
                  let sort =
                    if List.is_empty apps
                    then Elements.Base.(Ex Structure)
                    else Elements.Base.(Ex Functor) in
                  let Chain.Ex (_, p) = Chain._of_path sort p in
                  begin match Chain.drop p from_id with
                    | None ->
                      (* [p] should not be equal to [from_id] since the latter
                         refers to a value and the former to a module. *)
                      assert false
                    | _ -> ()
                    | exception Invalid_argument _ -> ()
                  end
              | _ -> ()
            end ;
            let locals = self#process_functor_application incl_mod in
            let nonlocals = super#visit_tt_include_declaration env decl in
            self#plus locals nonlocals

          method! visit_tt_module_binding env ({ mb_expr; _ } as mb) =
            let locals = self#process_functor_application mb_expr in
            let nonlocals = super#visit_tt_module_binding env mb in
            self#plus locals nonlocals

          method private process_functor_application me =
            match Module.unwind_expr me with
            | { mod_desc = Tmod_ident (p, _); _ } as me, (_::_ as apps), tys ->
              let p =
                Env.normalize_module_path (Some me.mod_loc) me.mod_env p in
              let head = Path.head p in
              let p =
                if Ident.persistent (Path.head p)
                then snd (Buildenv.Factorise.path p)
                else p in
              if not (Ident.same ident head) then
                self#zero
              else
                let p = Chain.of_path Functor p in
                begin match Chain.drop p from_id with
                  | None ->
                    (* [p] should not be equal to [from_id] since the latter
                       refers to a value and the former to a module. *)
                    assert false
                  | Some ((Chain.InParameter (Indexed idx, tl)) as c)
                    when idx < List.length apps ->
                    List.foldi
                      (fun reps i (me, tys) ->
                         let reps =
                           if i < idx then
                             let c = Chain.demote ~by:(i+1) c in
                             let visit =
                               Local.visitor#visit_tt_module_type
                                 (new Local.env c (Modulescope.empty)) in
                             List.fold_left (Fun.flip (visit %> self#plus))
                               reps
                               tys
                           else reps in
                         if i+1 = idx then
                           self#plus reps
                             Local.(
                               visitor#visit_tt_module_expr
                                 (new env tl Modulescope.empty)
                                 (me))
                         else reps)
                      (self#zero)
                      (apps)
                  | _ ->
                    self#zero
                  | exception Invalid_argument _ ->
                    self#zero
                end
            | _ ->
              self#zero
              (* N.B. No need to do nonlocal renaming in signatures. *)
        end
    end
    and

      Local : sig
      include module type of struct
        class ['a] env id scope = object (self)
          inherit ['a] id_env id
          inherit module_scope_env scope
        end
        let visitor : < visit_tt_structure : _value env -> _ ; .. > =
          object(self)
            inherit [_] reduce_with_id
            inherit Replacement.Set.monoid
          end
      end
    end = struct

      class ['a] env id scope = object (self)
        inherit ['a] id_env id
        inherit module_scope_env scope
      end

      let pattern_var_loc id =
        let reducer =
          object(self)
            inherit [_] Opt_reducers.leftmost
            method! visit_Tpat_var id id' { loc; _ } =
              if (Ident.same id id') then
                Some (`Regular loc)
              else
                None
            method! visit_Tpat_alias id pat as_id { loc; _ } =
              if (Ident.same id as_id) then
                Some (`Regular loc)
              else
                self#visit_tt_pattern id pat
            method! visit_Tpat_record id flds _ =
              flds |> List.find_map @@ fun ({ txt; loc; }, _, pat) ->
              (self#visit_tt_pattern id pat) |> Option.map @@
              function
              | `Regular l when Location.equal l loc ->
                `Punned ((longident_to_string txt), l)
              | x -> x
          end in
        Option.get_exn % reducer#visit_tt_pattern id

      let visitor =
        object(self)

          inherit [_] reduce_with_id as super
          inherit Replacement.Set.monoid

          method! process_binding_from_struct env this
              (type b)
              ((id : (Ident.t, b) Atom.t), _)
              (((_, _, InStr { str_env; str_loc; }, binding) as source)
               : (b, impl) binding_source) =
            let locals =
              match id, binding with
              | _, `Binding Value (_, InStrPrim { val_name = { loc; _}; _ }) ->
                (* Replacement.Set.of_opt None *)
                failwith "TODO: InstrPrim"
              (* Let-bindings: *)
              | Atom.Value id, `Binding Value (_, InStrVal {vb_pat; vb_expr}) ->
                begin match pattern_var_loc id vb_pat with
                  | `Regular loc ->
                    let args = retrieve_args vb_expr in
                    let max_arg = (max (arg_num_one ()) (arg_num_two ())) in
                    if List.length args < max_arg then
                      failwith "TODO: throw proper exception here";
                    let arg_one_loc = List.nth args ((arg_num_one ()) - 1) in
                    let arg_two_loc = List.nth args ((arg_num_two ()) - 1) in
                    let arg_one_txt = Sourcefile.extract_location
                        (InState.get_input ()) arg_one_loc in
                    let arg_two_txt = Sourcefile.extract_location
                        (InState.get_input ()) arg_two_loc in
                    Replacement.Set.union
                      (Replacement.Set.singleton
                         (Replacement.mk arg_one_loc arg_two_txt))
                      (Replacement.Set.singleton
                         (Replacement.mk arg_two_loc arg_one_txt))
                  | `Punned (lbl, loc) ->
                    failwith "TODO: punned bindings are not handled by swaparg"
                end
              | Atom.Value _, `Include InStr decl ->
                self#visit_tt_include_declaration env decl
              | _, `Binding Structure (_, InStr mb) ->
                self#visit_tt_module_binding env mb
              | _, `Binding StructureType InStr mtd ->
                self#visit_tt_module_type_declaration env mtd
              | _, `Binding Functor (_, InStr mb) ->
                self#visit_tt_module_binding env mb
              | _, `Binding FunctorType InStr mtd ->
                self#visit_tt_module_type_declaration env mtd
              | _, `Include InStr decl ->
                self#visit_tt_include_declaration env decl in
            let Atom.Data.{ id; _ } = Atom.unwrap id in
            let nonlocals =
              (Nonlocal.visitor (id, env#id))#visit_tt_structure `NO_PUN
                {this with
                 str_items =
                   let InStr rest = next_items source in rest } in
            self#plus locals nonlocals

          method! visit_tt_module_binding env mb =
            let id = Chain.tl_exn env#id in
            let scope, _ =
              Modulescope.enter_module_binding (InStr mb) env#scope in
            super#visit_tt_module_binding (new env id scope) mb

          method! visit_tt_include_declaration env decl =
            let scope, _ = Modulescope.enter_include (InStr decl) env#scope in
            super#visit_tt_include_declaration (env#with_scope scope) decl

          method! visit_tt_module_expr env ({ mod_loc; _ } as me) =
            let me, apps, tys = Module.unwind_expr me in
            let tys = List.fold_right (snd %> List.cons) apps [tys] in
            let tys = List.rev tys in
            let body_reps =
              match me.mod_desc with
              | Tmod_apply _
              | Tmod_constraint _ ->
                assert false
              | Tmod_ident _ ->
                self#zero
              | Tmod_functor (x, _, x_type, body) ->
                self#visit_functor env x x_type (InStr body)
              | Tmod_structure _struct ->
                self#visit_tt_structure env _struct
              | Tmod_unpack _ ->
                let () = Logging.warn @@ fun _f -> _f
                    ~tags:(of_loc mod_loc)
                    "module bound to an expression unpacking - cannot do local \
                     renaming!" in
                self#zero in
            let tys_reps =
              fst
                (List.fold_left
                   (fun (reps, env) tys ->
                      let reps =
                        List.fold_left
                          (Fun.flip
                             ((self#visit_tt_module_type env) %> self#plus))
                          (reps)
                          (tys) in
                      let env = env#with_id (Chain.promote env#id) in
                      reps, env)
                   (self#zero, env)
                   (tys)) in
            self#plus body_reps tys_reps

          method! process_binding_from_sig env this
              (type b)
              ((id : (Ident.t, b) Atom.t), _)
              ((_,_, InSig { sig_env; _ }, binding)
               : (b, intf) binding_source) =
            match id, binding with
            (* Val binding in signature: *)
            | _, `Binding Value (_, InSig { val_desc; _ }) ->
              let args = retrieve_types val_desc in
              if List.length args < (max (arg_num_one ()) (arg_num_two ())) then
                failwith "TODO: throw proper exception here";
              let loc_1 = get_nth_type_loc (arg_num_one ()) val_desc in
              let loc_2 = get_nth_type_loc (arg_num_two ()) val_desc in
              let open Sourcefile in
              let arg_one_str = extract_location (InState.get_input ()) loc_1 in
              let arg_two_str = extract_location (InState.get_input ()) loc_2 in
              Replacement.Set.union
                (Replacement.Set.singleton (Replacement.mk loc_1 (arg_two_str)))
                (Replacement.Set.singleton (Replacement.mk loc_2 (arg_one_str)))
            | Atom.Value _, `Include InSig { incl_mod; _ } ->
              (* This should handle anonymous signature includes *)
              self#visit_tt_module_type env incl_mod
            | _, `Binding Structure (_, InSig md) ->
              self#visit_tt_module_declaration env md
            | _, `Binding StructureType InSig mtd ->
              self#visit_tt_module_type_declaration env mtd
            | _, `Binding Functor (_, InSig md) ->
              self#visit_tt_module_declaration env md
            | _, `Binding FunctorType InSig mtd ->
              self#visit_tt_module_type_declaration env mtd
            | _, `Include InSig incl ->
              self#visit_tt_include_description env incl

          method! visit_tt_module_declaration env md =
            let id = Chain.tl_exn env#id in
            let scope, _ =
              Modulescope.enter_module_binding (InSig md) env#scope in
            super#visit_tt_module_declaration (new env id scope) md

          method! visit_tt_module_type_declaration env mtd =
            let id = Chain.tl_exn env#id in
            let scope, _ =
              Modulescope.enter_module_type_declaration mtd env#scope in
            super#visit_tt_module_type_declaration (new env id scope) mtd

          method! visit_tt_include_description env desc =
            let scope, _ = Modulescope.enter_include (InSig desc) env#scope in
            super#visit_tt_include_description (env#with_scope scope) desc

          method! visit_tt_module_type env this =
            match this.mty_desc with
            | Tmty_functor (x, _, x_type, body) ->
              self#visit_functor env x x_type (InSig body)
            | Tmty_typeof _ ->
              let () = Logging.warn @@ fun _f -> _f
                  ~tags:(of_loc this.mty_loc)
                  "module has type of a module expression - cannot do local \
                   renaming!" in
              self#zero
            | _ ->
              super#visit_tt_module_type env this

          method private visit_functor
            : 'a . _ -> _ -> _ -> 'a Typedtree_views.module_expr_view -> _ =
            fun env x x_type
              (type a) (body : a Typedtree_views.module_expr_view) ->
              let num_params =
                List.length (Modulescope.peek_params env#scope) + 1 in
              match Chain.hd env#id with
              | Atom.Ex Atom.Parameter (_, None) ->
                (* We expect identifier construction to force functor
                   parameters to be indexed. *)
                assert false
              | Atom.Ex Atom.Parameter (_, Some idx) when idx = num_params ->
                begin match x_type with
                  | None ->
                    failwith
                      (Format.sprintf
                         "%s.Reps.Local.visitor#visit_functor: cannot enter \
                          unit functor parameter"
                         __MODULE__)
                  | Some x_type ->
                    let id = Chain.tl_exn env#id in
                    let locals =
                      let scope = Modulescope.enter_param x env#scope in
                      self#visit_tt_module_type (new env id scope) x_type in
                    let nonlocals =
                      let id = Chain.cons (Atom.mk Elements.Base.Structure
                                             (Atom.Data.only (Ident.name x))) id
                      in
                      let visitor = Nonlocal.visitor (x, id) in
                      match body with
                      | InSig body ->
                        visitor#visit_tt_module_type `NO_PUN body
                      | InStr body ->
                        let () = Logging.debug @@ fun _f -> _f
                            "Entering functor body"
                        in
                        let e, tys = Module.unwrap_expr body in
                        let expr_nonlocals =
                          visitor#visit_tt_module_expr `NO_PUN e in
                        let tys_nonlocals =
                          self#visit_list visitor#visit_tt_module_type
                            `NO_PUN tys in
                        self#plus expr_nonlocals tys_nonlocals in
                    self#plus locals nonlocals
                end
              | _ ->
                match body with
                | InStr body ->
                  let _, tys = Module.unwrap_expr body in
                  let param = Option.map (Pair.make x) x_type in
                  let scope = Modulescope.push_param (param, tys) env#scope in
                  self#visit_tt_module_expr (env#with_scope scope) body
                | InSig body ->
                  let param = (Option.map (Pair.make x) x_type, []) in
                  let env =
                    env#with_scope (Modulescope.push_param param env#scope) in
                  self#visit_tt_module_type env body

        end

    end

  end

  let lookup_head_ident env cont =
    let from_id = fun_id () in
    let Atom.Ex hd = Chain.hd from_id in
    let path =
      lookup_path
        ~lookup_f:(fun lid -> Env.lookup_module ~load:true lid env)
        ?backup_lib:((InState.get_input ()).fileinfos.library)
        ~env
        (fun_lib (), Longident.Lident Atom.((unwrap hd).Data.id)) in
    match path with
    | None ->
      cont None
    | Some ((Path.Pident hd) as path) ->
      let { md_type; _ } = Env.find_module path env in
      let item =
        Moduletype.resolve_lookup env md_type (Chain.tl_exn from_id) in
      cont (Option.map (fun _ -> hd) item)
    | _ ->
      assert false

  let process_nonlocal env cont =
    lookup_head_ident env (Option.map_or ~default:Replacement.Set.empty cont)

  let process_sig local scope _sig =
    if local then
      let from_id = Chain.tl_exn (fun_id ()) in
      Reps.Local.(visitor#visit_tt_signature (new env from_id scope) _sig)
    else
      process_nonlocal
        (initial_env (Sig _sig))
        (fun ident ->
           let visitor = Reps.Nonlocal.visitor (ident, fun_id ()) in
           visitor#visit_tt_signature `NO_PUN _sig)

  let process_struct local scope _struct =
    if local then
      let from_id = Chain.tl_exn (fun_id ()) in
      let replacements =
        Reps.Local.
          (visitor#visit_tt_structure (new env from_id scope) _struct) in
      let () = if Replacement.Set.is_empty replacements then
          (* TODO: Consider: do we need to emit this warning? *)
          Logging.warn @@ fun _f -> _f
            "Could not find local value to extend in %s!"
            (InState.get_input ()).fileinfos.filename in
      replacements
    else
      process_nonlocal
        (initial_env (Str _struct))
        (fun ident ->
           let visitor = Reps.Nonlocal.visitor (ident, fun_id ()) in
           visitor#visit_tt_structure `NO_PUN _struct)

  let process_file input =
    let infos = input.fileinfos in
    let local = is_local infos in
    let mod_scope = ((module_name infos, None), []) in
    let () = InState.set_input input in
    InState.dispatch_on_input_type
      ~intf_f:(process_sig local mod_scope)
      ~impl_f:(process_struct local mod_scope)



  (* ------------------------------------------------------------------------
     DEPENDENCIES
     ------------------------------------------------------------------------ *)

  module Deps = Deps.Make (InState) (Param)

  let get_deps_nonlocal env cont =
    lookup_head_ident env (Option.map_or ~default:DepSet.empty cont)

  let get_sig_deps local scope _sig =
    if local then
      let from_id = Chain.tl_exn (fun_id ()) in
      Deps.Local.
        (visitor#visit_tt_signature (new env from_id scope None) _sig)
    else
      get_deps_nonlocal
        (initial_env (Sig _sig))
        (fun ident ->
           let visitor = Deps.Nonlocal.visitor (ident, fun_id ()) in
           visitor#visit_tt_signature scope _sig)

  let get_struct_deps local scope _struct =
    if local then
      let from_id = Chain.tl_exn (fun_id ()) in
      Deps.Local.
        (visitor#visit_tt_structure (new env from_id scope None) _struct)
    else
      get_deps_nonlocal
        (initial_env (Str _struct))
        (fun ident ->
           let visitor = Deps.Nonlocal.visitor (ident, fun_id ()) in
           visitor#visit_tt_structure scope _struct)

  let get_deps ~mli input =
    let infos = input.fileinfos in
    let local = is_local infos in
    let mod_scope =
      Option.map_or
        ~default:((module_name infos, None), []) initial_module_scope mli in
    let () = InState.set_input input in
    let deps =
      InState.dispatch_on_input_type
        ~intf_f:(get_sig_deps local mod_scope)
        ~impl_f:(get_struct_deps local mod_scope) in
    let _ = InState.get_input () in
    let () =
      if not (DepSet.is_empty deps) then
        Logging.info @@ fun _f -> _f
          ~header:"Deps:"
          "@[<v>%a@]" (DepSet.pp Refactoring_deps.Elt.pp) deps in
    deps
end
