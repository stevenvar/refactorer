open Containers

open Elements
open Elements.Base

open Identifier
open Refactoring_error

module ParameterState = struct

  module type S = sig
    type kind_t
    val kind : kind_t Base.t
    val initialise : string list -> unit
    val init : kind_t Identifier.t * string -> unit
    val from_lib : unit -> string option
    val from_id : unit -> kind_t Chain.t
    val to_id : unit -> string
    val to_repr : unit -> Refactoring_repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make
      (X : sig
         type kind_t
         val kind : kind_t Base.t
       end)
    : S with type kind_t = X.kind_t =
  struct

    type kind_t = X.kind_t

    let kind = X.kind

    let from_id : kind_t Identifier.t option ref = ref None
    let to_id : string option ref = ref None

    let validate ((_from_lib, _from_id), _to_id) =
      begin match _from_lib with
        | Some _lib_name
          when not (is_ident _lib_name) ->
          invalid_arg "Name to substitute must be a valid identifier!"
        | _ -> ()
      end ;
      begin if not (is_lowercase_ident _to_id) then
          invalid_arg "Name to substitute must be a valid identifier!"
      end

    let init ((id, _to) as params) =
      validate params ;
      from_id := Some id ;
      to_id := Some _to

    let initialise args =
      if List.length args < 2 then
        invalid_arg "Not enough parameters!"
      else
        let (_from_lib, _from_id) = of_string (List.nth args 0) in
        let _from_id =
          try
            Chain.try_open X.kind _from_id
          with KindMismatch msg ->
            invalid_arg
              (Format.sprintf
                 "Invalid identifier for substitution: %s" msg) in
        let _to_id = List.nth args 1 in
        init ((_from_lib, _from_id), _to_id)

    (*** Getters ***)

    let from_lib () =
      Option.flat_map fst !from_id

    let from_id () = match !from_id with
      | Some (_, id) -> id
      | None ->
        let err = Not_initialised "Identifier to substitute for not specified!" in
        raise (Error (err, None))

    let to_id () = match !to_id with
      | Some id -> id
      | None ->
        let err = Not_initialised "Identifier to substitute not specified!" in
        raise (Error (err, None))

    (*** Ancillary ***)

    let is_local input =
      let open Fileinfos in
      let libs_match =
        match input.library, (from_lib ()) with
        | None, None -> true
        | Some lib, Some lib' ->
          String.equal
            (String.capitalize_ascii lib)
            (String.capitalize_ascii lib')
        | _ -> false in
      let modules_match =
        let open Chain in
        match from_id () with
        | InStructure (m, _) ->
          String.equal m (module_name input)
        | _ -> false in
      libs_match && modules_match

    (*** Implementations for Refactoring Interface ***)

    let to_repr () =
      Refactoring_repr.Rename ((from_lib (), Chain.Ex (kind, from_id ())), to_id ())

    let kernel_mem f = is_local f

    let kernel c =
      let c = Codebase.filter is_local c in
      if Codebase.is_empty c then
        let err =
          KernelError
            (Format.sprintf "kernel for %a is empty" Refactoring_repr.pp (to_repr ())) in
        raise (Error (err, None))
      else
        Codebase.to_list c
        (* TODO: Find a more efficient way of doing this *)

  end

end