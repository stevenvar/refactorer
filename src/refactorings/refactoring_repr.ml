open Containers

open Lib

module type S = sig
  type t =
    | AlphaRename of Identifier._t * int * int * string 
    | Identity
    | Rename of Identifier._t * string
    | Reduce of unit
    | Addarg of Identifier._t * string * string * string
    | Swaparg of Identifier._t * int * int
    | Inline of Identifier._t * bool * Codebase.t
  val pp : t Format.printer
  val to_string : t -> string
  val compare : t -> t -> int
  val equal : t -> t -> bool
  module Set : SetWithMonoid.S with type elt = t
  val identity : t
end

module Orderable = struct
  type t =
    | AlphaRename of Identifier._t * int * int * string 
    | Identity
    | Rename of Identifier._t * string
    | Reduce of unit
    | Addarg of Identifier._t * string * string * string
    | Swaparg of Identifier._t * int * int
    | Inline of Identifier._t  * bool * Codebase.t
  let compare = Stdlib.compare
  (* TODO: remove use of polymorphic compare? *)
  let equal t t' =
    compare t t' = 0
end
include Orderable
module Set = SetWithMonoid.Make(Orderable)
let pp fmt = function
  | AlphaRename (((_, (Identifier.Chain.Ex (kind, _))) as id), lnum, cnum,new_name )  -> 
    Format.fprintf fmt "@[Alpha-rename %a:%a (%d,%d) -> %s @]"
      Elements.Base.pp kind
      Identifier._pp id
      lnum
      cnum
      new_name
  | Identity -> Format.fprintf fmt "@[Identity@]"
  | Rename (((_, (Identifier.Chain.Ex (kind, _))) as id), _to) ->
    Format.fprintf fmt "@[Rename %a: %a -> %s@]"
      Elements.Base.pp kind
      Identifier._pp id
      _to
  | Reduce () ->
    Format.fprintf fmt "@[Reduce @]"
  | Addarg (((_, (Identifier.Chain.Ex (kind, _))) as id), arg_name, typ, default_value) ->
    Format.fprintf fmt "@[Extend %a:%a + (%s : %s = %s)@]"
      Elements.Base.pp kind
      Identifier._pp id
      arg_name
      typ
      default_value
  | Swaparg (((_, (Identifier.Chain.Ex (kind, _))) as id), arg_num_one, arg_num_two) ->
    Format.fprintf fmt "@[Swap %a:%a : %d <-> %d @]"
      Elements.Base.pp kind
      Identifier._pp id
      arg_num_one
      arg_num_two
  | Inline (((_, (Identifier.Chain.Ex (kind, _))) as id), mark_reduce, codebase) ->
    Format.fprintf fmt "@[Inline %a:%a mark_reduce = %b @]"
      Elements.Base.pp kind
      Identifier._pp id
      mark_reduce

let to_string = mk_to_string pp
let identity = Identity
let rename (id, name) = Rename (id, name)
let reduce (id, lnum, cnum, all) = Reduce  ()
let addarg (id, arg, typ, default) = Addarg (id, arg, typ, default)
let swaparg (id, arg_num_one, arg_num_two) = Swaparg (id, arg_num_one, arg_num_two)
let inline (id,red,cb) = Inline (id,red,cb)
