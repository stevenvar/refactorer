open Containers
open   IO
open   Fun

open Compiler
open   Compmisc
open   Config

open Rotor
open   Refactoring
open   Refactoring_error
open   Refactoring_sigs
open   Lib
open   Sourcefile
open   Fileinfos

open Frontend

let apply_refactoring_to_codebase r (codebase,filedeps) rdeps_file =
  try
    let (deps, results) = apply r codebase ~filedeps in
    let () = Logging.info @@ fun _f -> _f
        ~header:"Refactoring Set"
        "@[<v>%a@]" (Repr.Set.pp Repr.pp) (Deps.dependents deps) in
    let () =
      match rdeps_file with
      | None -> ()
      | Some f ->
        with_out f @@ fun f ->
        Format.fprintf (Format.formatter_of_out_channel f)
          "@[<v>%a@]@." Deps.pp deps in
    let () =
      Logging.major_progress
        ~msgf:(fun _f -> _f "Requires changes to %i files"
                  (Fileinfos.Map.cardinal results)) () in
    results |> Fileinfos.Map.iter @@ fun f results ->
    let src_file = Sourcefile.of_fileinfos f in
    let () =
      Logging.debug @@ fun _f -> _f
        "Performing the following replacements in %s:@,@[%a@]"
        f.filename (Replacement.Set.pp Replacement.pp) results in
    let results = Replacement.apply_all results src_file.contents in
    let results = Sourcefile.diff src_file results in
    let () =
      Logging.info @@ fun _f -> _f
        "Computed patch for %s" f.filename in
    if String.length results <> 0 then
      Output.print_endline results
  with
  | Error (err, _) ->
    let () = log_error err in
    failwith
      (Format.sprintf "%a" print_error err)

let addarg fun_name arg_name typ default_value (codebase, filedeps) rdeps_file =
  let r = Repr.Addarg (fun_name,arg_name,typ,default_value) in
  apply_refactoring_to_codebase r (codebase,filedeps) rdeps_file

let alpha_rename from_id lnum cnum new_name (codebase, filedeps) rdeps_file =
  let r = Repr.AlphaRename (from_id, lnum, cnum,new_name) in
  apply_refactoring_to_codebase r (codebase,filedeps) rdeps_file

let reduce (codebase, filedeps) rdeps_file =
  let r = Repr.Reduce () in
  apply_refactoring_to_codebase r (codebase,filedeps) rdeps_file

let rename from_id to_id (codebase, filedeps) rdeps_file =
  let r = Repr.Rename (from_id, to_id) in
  apply_refactoring_to_codebase r (codebase,filedeps) rdeps_file

let swaparg fun_name arg_num_one arg_num_two (codebase, filedeps) rdeps_file =
  let r = Repr.Swaparg (fun_name,arg_num_one,arg_num_two) in
  apply_refactoring_to_codebase r (codebase,filedeps) rdeps_file

let inline from_id mark_reduce (codebase, filedeps) rdeps_file =
  let r = Repr.Inline (from_id,mark_reduce,codebase) in
  apply_refactoring_to_codebase r (codebase,filedeps) rdeps_file

let mod_deps (_, deps) =
  let process_item f =
    let module_name = Fileinfos.cmt_module_name f in
    let sort =
      Option.get_exn
        (String.chop_prefix ~pre:"."
           (Filename.extension (Fileinfos.filename f))) in
    let deps =
      List.map Fileinfos.cmt_module_name (Fileinfos.Graph.succ deps f) in
    let deps = List.sort_uniq String.compare deps in
    Output.print_endline
      (Format.sprintf "@[<h>%s:%s %a@]"
         module_name sort
         (List.pp ~start:"[ " ~stop:" ]" ~sep:" " Format.string) deps) in
  Fileinfos.Graph.iter_vertex process_item deps

let tool_name =
  Filename.basename Sys.executable_name

(* Command Line *)

let dep_file =
  let docs = Cmdliner.Manpage.s_options in
  let doc = "Save refactoring dependencies in $(docv)" in
  let docv = "FILE" in
  let arg_info = Cmdliner.Arg.info ~docs ~doc ~docv ["rdeps"] in
  Cmdliner.Arg.(value & opt (some string) None arg_info)

let addarg_term =
  let fun_name =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier of the function to be extended." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        try
          Ok (Identifier.of_string s)
        with Failure s | Invalid_argument s ->
          Error (`Msg s) in
      Cmdliner.Arg.conv (parser, Identifier._pp) in
    Cmdliner.Arg.(required & pos 0 (some id_conv) None arg_info) in
  let arg_name =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The new argument name." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        if Identifier.is_lowercase_ident s then Ok s
        else Error (`Msg "The new argument name must be a valid identifier!") in
      Cmdliner.Arg.conv (parser, Format.string) in
    Cmdliner.Arg.(required & pos 1 (some id_conv) None arg_info) in
  let typ =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The new argument type." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s = Ok s in
      Cmdliner.Arg.conv (parser, Format.string) in
    Cmdliner.Arg.(required & pos 2 (some id_conv) None arg_info) in
  let default_value =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The new argument default value." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s = Ok s in
      Cmdliner.Arg.conv (parser, Format.string) in
    Cmdliner.Arg.(required & pos 3 (some id_conv) None arg_info) in
  Cmdliner.Term.(
    const addarg
    $ fun_name
    $ arg_name
    $ typ
    $ default_value
    $ (Codebase.of_cmdline_with_deps $ const tool_name)
    $ dep_file)
let addarg_info =
  let doc = "Extends a function with a new argument." in
  Cmdliner.Term.info ~doc "addarg"


let alpha_rename_term =
  let from_id =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier of the file where to apply alpha-renaming." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        try
          Ok (Identifier.of_string s)
        with Failure s | Invalid_argument s ->
          Error (`Msg s) in
      Cmdliner.Arg.conv (parser, Identifier._pp) in
    Cmdliner.Arg.(required & pos 0 (some id_conv) None arg_info) in
  let line_num =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The line number of where to apply alpha-renaming." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    Cmdliner.Arg.(required & pos 1 (some int) None arg_info) in
  let column_num =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The column number of where to apply alpha-renaming" in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    Cmdliner.Arg.(required & pos 2 (some int) None arg_info) in
  let new_name =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier to rename to." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        if Identifier.is_lowercase_ident s then Ok s
        else Error (`Msg "Must rename to a valid identifier!") in
      Cmdliner.Arg.conv (parser, Format.string) in
    Cmdliner.Arg.(required & pos 3 (some id_conv) None arg_info) in
  Cmdliner.Term.(
    const alpha_rename
    $ from_id
    $ line_num
    $ column_num
    $ new_name
    $ (Codebase.of_cmdline_with_deps $ const tool_name)
    $ dep_file)
let alpha_rename_info =
  let doc = "Alpha-renames an identifier" in
  Cmdliner.Term.info ~doc "alpha-rename"

let rename_term =
  let from_id =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier of the value to be renamed." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        try
          Ok (Identifier.of_string s)
        with Failure s | Invalid_argument s ->
          Error (`Msg s) in
      Cmdliner.Arg.conv (parser, Identifier._pp) in
    Cmdliner.Arg.(required & pos 0 (some id_conv) None arg_info) in
  let to_id =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier to rename to." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        if Identifier.is_lowercase_ident s then Ok s
        else Error (`Msg "Must rename to a valid identifier!") in
      Cmdliner.Arg.conv (parser, Format.string) in
    Cmdliner.Arg.(required & pos 1 (some id_conv) None arg_info) in
  Cmdliner.Term.(
    const rename
    $ from_id
    $ to_id
    $ (Codebase.of_cmdline_with_deps $ const tool_name)
    $ dep_file)
let rename_info =
  let doc = "Renames a value." in
  Cmdliner.Term.info ~doc "rename"

let reduce_term =
  (* let all = Cmdliner.Arg.(value & flag (info ["all"]  )) in *)
  Cmdliner.Term.(
    const reduce
    $ (Codebase.of_cmdline_with_deps $ const tool_name)
    $ dep_file)
let reduce_info =
  let doc = "Beta-reduces an application." in
  Cmdliner.Term.info ~doc "reduce"

let swaparg_term =
  let fun_name =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier of the function to swap the arguments in." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        try
          Ok (Identifier.of_string s)
        with Failure s | Invalid_argument s ->
          Error (`Msg s) in
      Cmdliner.Arg.conv (parser, Identifier._pp) in
    Cmdliner.Arg.(required & pos 0 (some id_conv) None arg_info) in
  let arg_num_one =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The first argument to swap." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    Cmdliner.Arg.(required & pos 1 (some int) None arg_info) in
  let arg_num_two =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The second argument to swap." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    Cmdliner.Arg.(required & pos 2 (some int) None arg_info) in
  Cmdliner.Term.(
    const swaparg
    $ fun_name
    $ arg_num_one
    $ arg_num_two
    $ (Codebase.of_cmdline_with_deps $ const tool_name)
    $ dep_file)
let swaparg_info =
  let doc = "Swaps the order of two arguments in a function definition." in
  Cmdliner.Term.info ~doc "swaparg"

let inline_term =
  let fun_name =
    let docs = Cmdliner.Manpage.s_arguments in
    let doc = "The identifier of the function to inline." in
    let arg_info = Cmdliner.Arg.info ~docs ~doc [] in
    let id_conv =
      let parser s =
        try
          Ok (Identifier.of_string s)
        with Failure s | Invalid_argument s ->
          Error (`Msg s) in
      Cmdliner.Arg.conv (parser, Identifier._pp) in
    Cmdliner.Arg.(required & pos 0 (some id_conv) None arg_info) in
  let reduce = Cmdliner.Arg.(value & flag (info ["mark-reduce"]  )) in
  Cmdliner.Term.(
    const inline
    $ fun_name
    $ reduce
    $ (Codebase.of_cmdline_with_deps $ const tool_name)
    $ dep_file)
let inline_info =
  let doc = "Locally inlines a function definition." in
  Cmdliner.Term.info ~doc "inline"

let mod_deps_term =
  let open Cmdliner.Term in
  const mod_deps $ (Codebase.of_cmdline_with_deps $ const tool_name)
let mod_deps_info =
  let doc = "Output module dependencies of input codebase." in
  Cmdliner.Term.info ~doc "mod-deps"

let cmds = [
  (alpha_rename_term, alpha_rename_info);
  (addarg_term, addarg_info);
  (inline_term, inline_info);
  (reduce_term, reduce_info);
  (rename_term, rename_info) ;
  (swaparg_term, swaparg_info) ;
  (mod_deps_term, mod_deps_info) ;
]

let cmds = List.map (Pair.map1 (with_common_opts)) cmds

let help_secs = [
  `S Cmdliner.Manpage.s_common_options;
  `P "These options are common to all commands.";
  `S "MORE HELP";
  `P "Use `$(mname) $(i,COMMAND) --help' for help on a single command.";`Noblank;
  `S Cmdliner.Manpage.s_bugs;
  `P "Check bug reports at https://gitlab.com/trustworthy-refactoring/refactorer/issues.";
]

let default_cmd =
  let doc = "a refactoring tool for OCaml" in
  let sdocs = Cmdliner.Manpage.s_common_options in
  let exits = Cmdliner.Term.default_exits in
  let man = help_secs in
  let version = Configuration.version in
  Cmdliner.Term.(
    ret (const (fun _ -> `Help (`Pager, None))
         $ with_common_opts (Codebase.of_cmdline_with_deps $ const tool_name))),
  Cmdliner.Term.info tool_name ~version ~doc ~sdocs ~exits ~man

let () = Cmdliner.Term.(exit @@ eval_choice default_cmd cmds)
