open Containers
open Fun
open Compiler
open Types
open Elements
open Elements.Module
open Elements.ModuleType
open Identifier

let unwrap mt =
  let rec unwrap mt acc =
    match mt.Typedtree.mty_desc with
    | Typedtree.Tmty_with (mt, cs) ->
      unwrap mt (cs :: acc)
    | _ ->
      mt, acc
  in
  unwrap mt []

let module_type_consistent_with m mt =
  match m, mt with
  | Elements.Module.(Ex S), Elements.ModuleType.(Ex ST) ->
    true
  | Elements.Module.(Ex F), Elements.ModuleType.(Ex FT) ->
    true
  | _
    -> false

let resolve env =
  let rec resolve  =
    function
    | Mty_signature _sig ->
      Types_views.(Ex (ST _sig))
    | Mty_functor (x, x_sig, _sig) ->
      Types_views.(Ex (FT (x, x_sig, _sig)))
    | Mty_ident p ->
      begin match Env.find_modtype p env with
        | { mtd_type = Some mt; _ } ->
          resolve mt
        | _ ->
          raise Not_found
      end
    | Mty_alias _ as mty ->
      let p = get_module_type_path mty in
      let { md_type; _ } = Env.find_module p env in
      resolve md_type
  in resolve

let sort env mt =
  match resolve env mt with
  | Types_views.(Ex ST _) -> Some ModuleType.(Ex ST)
  | Types_views.(Ex FT _) -> Some ModuleType.(Ex FT)
  | exception Not_found -> None

module Binding = struct
  let sort =
    function
    | Compiler.Typedtree.{ mtd_type = Some { mty_type; mty_env; _ }; _ } ->
      sort mty_env mty_type
    | m ->
      Format.printf "Name = %s / location = %a \n" m.mtd_name.txt Location.print_loc m.mtd_loc;
      None
end

open Types_views

let resolve_view env =
  let rec resolve_view : 'a . 'a module_type_view -> 'a module_type =
    fun (type a) (mtv : a module_type_view) : a module_type ->
      match mtv with
      | Mty_immediate mt ->
        mt
      | Mty_ident (sort, p) ->
        let Compiler.Types.{ mtd_type; _ } = Env.find_modtype p env in
        begin match mtd_type with
          | None ->
            let () =
              Logging.warn @@ fun _f -> _f
                ~tags:
                  (Logging.Tags.preamble
                     (Format.sprintf "%s.resolve" __MODULE__))
                "No module type found for %a" Printtyp.path p in
            raise Not_found
          | Some mt ->
            let mtv = mk_module_type_view (sort, mt) in
            resolve_view mtv
        end
      | Mty_alias (sort, p) ->
        (* let () = Logging.debug @@ fun _f -> _f
           "Looking for alias path %a" Printtyp.path p in *)
        let Compiler.Types.{ md_type; _ } = Env.find_module p env in
        let mtv = mk_module_type_view (sort, md_type) in
        resolve_view mtv in
  resolve_view

let lookup env =
  let rec lookup
    : 'a 'b . 'a module_type -> 'b Chain.t -> 'b item_element_view Option.t =
    fun (type a b) (mt : a module_type) (id : b Chain.t) ->
      let process_sig_item item : b item_element_view Option.t =
        match id with
        | Chain.Atomic a ->
          mk_item_view a item
        | _ ->
          let Atom.Ex a = Chain.hd id in
          match mk_item_view a item with
          | Some (Value _) ->
            invalid_arg
              (Format.sprintf
                 "%s.ty_sig_lookup: cannot recurse into a value!"
                 __MODULE__)
          | Some (Structure (_, { md_type; _ }, _)) ->
            begin try
                lookup (resolve_view env md_type) (Chain.tl_exn id)
              with
              | Not_found ->
                None
            end
          | Some (Functor (_, { md_type; _ }, _)) ->
            begin try
                lookup (resolve_view env md_type) (Chain.tl_exn id)
              with
              | Not_found ->
                None
            end
          | Some (StructureType (_, { mtd_type = Some mtv; _ })) ->
            begin try
                lookup (resolve_view env mtv) (Chain.tl_exn id)
              with
              | Not_found ->
                None
            end
          | Some (FunctorType (_, { mtd_type = Some mtv; _ })) ->
            begin try
                lookup (resolve_view env mtv) (Chain.tl_exn id)
              with
              | Not_found ->
                None
            end
          | _ ->
            None in
      (* TODO: Mismatched, erroneous cases *)
      match mt, id with
      | FT (_, x_sig, _sig), Chain.InParameter (Indexed idx, tail) ->
        if idx - 1 = 0 then
          let Ex mtv = resolve env (Option.get_exn x_sig) in
          lookup mtv tail
        else
          let Ex mtv = resolve env _sig in
          lookup mtv (Chain.demote id)
      | FT (_, _, _sig), _ ->
        let Ex mtv = resolve env _sig in
        lookup mtv id
      | ST _sig, _ ->
        _sig
        |> List.rev (* Get bottom-most match *)
        |> List.find_map process_sig_item in
  lookup

let _lookup env (Types_views.Ex mt) c =
  lookup env mt c

let resolve_lookup env mty id =
  let Ex mty = resolve env mty in
  lookup env mty id

let find (type a)  (m : (_, a) Atom.t) env =
  let mk_modtype (type a) (sort : (a Elements.ModuleType.sub_t)) mt =
    resolve_view env (Types_views.mk_module_type_view (sort, mt)) in
  let Atom.Data.{ id; _ } = Atom.unwrap m in
  let p = Path.Pident id in
  let open Types in
  try match m with
    | Atom.Structure _ ->
      let { md_type; _ } = Env.find_module p env in
      Some (Ex (mk_modtype Elements.ModuleType.ST md_type))
    | Atom.Functor _ ->
      let { md_type; _ } = Env.find_module p env in
      Some (Ex (mk_modtype Elements.ModuleType.FT md_type))
    | Atom.StructureType _ ->
      let { mtd_type; _ } = Env.find_modtype p env in
      Option.map
        (fun mt -> Ex (mk_modtype Elements.ModuleType.ST mt))
        (mtd_type)
    | Atom.FunctorType _ ->
      let { mtd_type; _ } = Env.find_modtype p env in
      Option.map
        (fun mt -> Ex (mk_modtype Elements.ModuleType.FT mt))
        (mtd_type)
    | _ ->
      invalid_arg
        (Format.sprintf "%s.find: %a" __MODULE__ (Atom.pp Ident.print) m)
  with Not_found ->
    None

let find_lookup m env c =
  Option.flat_map (fun mt -> _lookup env mt c) (find m env)

let contains env mt atm =
  let Atom.Data.{ id; _ }, _ = Atom.dest atm in
  Option.map_or ~default:false (Fun.compose item_ident (Ident.same id))
    (lookup env mt (Chain.mk (Atom.map (fun id -> Ident.name id) atm)))

let rec find_param_by_id env id =
  function
  | Ex (ST _) ->
    invalid_arg (Format.sprintf "%s.find_param_by_id" __MODULE__)
  | Ex (FT (x, x_type, body))
    when Ident.same id x ->
    Some x_type
  | Ex (FT (x, x_type, body)) ->
    match resolve env body with
    | Ex (ST _) ->
      None
    | mty ->
      find_param_by_id env id mty

let find_param_by_idx env idx =
  let rec find_param_by_idx env idx' idx =
    function
    | Ex (ST _) ->
      invalid_arg (Format.sprintf "%s.find_param_by_idx" __MODULE__)
    | Ex (FT (x, x_type, body))
      when idx = idx' ->
      Some (x, x_type)
    | Ex (FT (x, x_type, body)) ->
      match resolve env body with
      | Ex (ST _) ->
        None
      | mty ->
        find_param_by_idx env (idx'+1) idx mty
  in
  find_param_by_idx env 1 idx
