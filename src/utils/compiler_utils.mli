open Containers

open Compiler

val log_error : ?tags:Logs.Tag.set -> Location.error -> unit

val error_of_exn : exn -> Location.error
(** [error_of_exn e] returns [err] if [Location.error_of_exn e] is [Some err]
    and raises [Error_not_recognised] otherwise. The idea is that we use this
    function to get the [Location.error] value to report after having caught
    a concrete compiler exception that we expect [Location.error_of_exn] to
    recognise. *)

val get_raw_backtrace : unit -> Printexc.raw_backtrace option
(** Returns [Some b], where [b] is the backtrace of the most recently raised
    exception, if backtraces are recorded and returns [None] otherwise. *)

exception Error_not_recognised of exn * Printexc.raw_backtrace option