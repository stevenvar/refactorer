open Containers
open   Format

open Compiler

open Location

let log_error ?tags err =
  Logging.warn @@ fun _f-> _f
    ?tags ~header:"Compiler Error" "%a" pp_error err

exception Error_not_recognised of exn * Printexc.raw_backtrace option

let get_raw_backtrace () =
  if Printexc.backtrace_status () then
    Some (Printexc.get_raw_backtrace ())
  else
    None

let error_of_exn e =
  match err_of_exn e with
  | Some err -> err
  | None -> raise (Error_not_recognised (e, get_raw_backtrace ()))

let () =
  Printexc.register_printer @@ function
  | Error_not_recognised (e, b) ->
    Some
      (Format.sprintf "Error_not_recognised: %s@.%a"
         (Printexc.to_string e)
         (some (of_to_string Printexc.raw_backtrace_to_string)) b)
  | _ -> None