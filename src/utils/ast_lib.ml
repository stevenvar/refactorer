open Containers

open Compiler

open Asttypes
open Location
open Lexing

open Typedtree

open Lib

let location_to_string = mk_to_string Location.print_loc

let longident_to_string = mk_to_string Printtyp.longident

let path_to_string = mk_to_string Printtyp.path

let cmp_loc
    { loc_start = { pos_cnum = loc1; _ }; _ }
    { loc_start = { pos_cnum = loc2; _ }; _ } =
  loc1 - loc2

let cmp_by_loc e e' = cmp_loc e.exp_loc e'.exp_loc

(* Check the location is in the range of the source string *)
let check_loc { loc_start; loc_end; _ } lines =
  let start_col = loc_start.pos_cnum - loc_start.pos_bol in
  let end_col = loc_end.pos_cnum - loc_end.pos_bol in
  0 < loc_start.pos_lnum && loc_start.pos_lnum <= Array.length lines
  &&
  0 < loc_end.pos_lnum && loc_end.pos_lnum <= Array.length lines
  &&
  0 <= start_col && start_col <= String.length lines.(loc_start.pos_lnum - 1)
  &&
  0 < end_col && end_col <= String.length lines.(loc_end.pos_lnum - 1)

let is_arg_pun preamble = function
  | Labelled lbl, { exp_desc = Texp_ident (_, { txt; _ }, _); _ } ->
    String.equal lbl (longident_to_string txt) &&
    not (String.mem (strip_comments preamble) ~sub:(Tokens.mk_arg_label lbl))
  | Optional lbl,
    { exp_desc = Texp_construct (_, { Types.cstr_name; _},
                                 [ { exp_desc = Texp_ident (_, { txt; _ }, _); _ } ] ); _ }
    when String.equal cstr_name "Some" ->
    String.equal lbl (longident_to_string txt)
  | _ -> false

let is_field_pun preamble _ =
  not (String.contains (strip_comments preamble) '=')
