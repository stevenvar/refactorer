open Compiler
open Containers

val mk_to_string : (Format.formatter -> 'a -> 'b) -> 'a -> string

val output_warning : ?loc:Location.t -> string -> unit

val not_implemented : unit -> 'a

module Tokens : sig
  val space : char
  val labelled_arg_signifier : char
  val arg_label_terminator : char
  val comment_begin : string
  val comment_end : string
  val brackets : string * string
  val mk_arg_label : ?signifier:bool -> string -> string
  val mk_field_label : string -> string
end

module ApplyTokens : sig
  val brackets : (string -> string -> 'a) -> 'a
end

val strip_comments : string -> string
val lines_between :
  Lexing.position * Lexing.position -> string Array.t -> string
val lines_between_buf :
  Lexing.position * Lexing.position -> string Array.t -> Buffer.t -> Buffer.t

(*** FORMATTING ***)
val after_char : char -> 'a Format.printer -> 'a Format.printer

(*** FILENAME MANIPULATION ***)
val get_mli : string -> string
(** [get_mli f] strips the filename [f] of its extension and appends the
    interface file extension, as taken from [Config.interface_suffix]
    (i.e. "mli"). *)

(*** FILE SYSTEM ***)
val tmp_dir : string -> string

(*** PARSING ***)
val mk_of_string : ('a, unit) MParser.t -> string -> 'a
val mk_of_channel : ('a, unit) MParser.t -> in_channel -> 'a

(*** LIST FUNCTIONS ***)
val pivot_map : ?rev:bool -> ('a -> 'b option) -> 'a list -> ('a list * 'b * 'a list) option
val pivot_pred : ?rev:bool -> ('a -> bool) -> 'a list -> ('a list * 'a * 'a list) option

(*** Utility Modules and Functors ***)
module OrderedPair
    (X : sig type t val compare : t -> t -> int end)
    (Y : sig type t val compare : t -> t -> int end)
  : sig
      type t
      val compare : t -> t -> int
    end with type t = X.t * Y.t
(** A functor to create [OrderedType] Pairing of  two [OrderedType]s *)

module SetWithMonoid : sig
  module type S = sig
    include Set.S
    class monoid : object
      method private zero : t
      method private plus : t -> t -> t
    end
    (** A class that represents the monoid on this Set interface. This class is
        designed to be inherited by reduce visitors on ASTs. *)
  end
  (** This interface adds a class to the [Set.S] interface that implements
      monoid operations. *)
  module Make(X : Set.OrderedType) : S with type elt = X.t
end
(** A module to represent sets with a class with methods implementing a monoid. *)