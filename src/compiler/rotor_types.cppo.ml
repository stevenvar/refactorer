open Containers

open Compiler_import

include Types

let get_module_type_path =
  function
  | Mty_ident p
#if OCAML_MINOR < 8
  | Mty_alias (_, p) ->
#else
  | Mty_alias p ->
#endif
    p
  | _ ->
    invalid_arg (Format.sprintf "%s.get_module_type_path" __MODULE__)

let get_sig_value_id = function
#if OCAML_MINOR < 8
  | Sig_value  (id,_) ->
#else
  | Sig_value (id,_,_) ->
#endif
id
  | _ -> failwith "not a sig value"

  let get_sig_module_id = function
#if OCAML_MINOR < 8
  | Sig_module  (id,_,_) ->
#else
  | Sig_module (id,_,_,_,_) ->
#endif
id
  | _ -> failwith "not a sig module"

  let get_sig_modtype_id = function
  #if OCAML_MINOR < 8
    | Sig_modtype (id,_) ->
  #else
    | Sig_modtype (id,_,_) ->
  #endif
  id
    | _ -> failwith "not a sig modtype"
  
    let get_sig_type_id = function
    #if OCAML_MINOR < 8
      | Sig_type (id,_,_) ->
    #else
      | Sig_type (id,_,_,_) ->
    #endif
    id
      | _ -> failwith "not a sig modtype"
    