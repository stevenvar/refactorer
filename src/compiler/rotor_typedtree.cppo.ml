open Compiler_import

include Typedtree

let env_of_exp_extra =
  function
  #if OCAML_MINOR < 8
  | Texp_open (_, _, _, env) -> Some env
                                     #endif
  | _ -> None

(* Accessors for different nodes of the AST that changed between versions: *)

let get_match_expression = function
  #if OCAML_MINOR < 8
  | Texp_match (e,_,_,_) ->
    #else
  | Texp_match (e,_,_) ->
    #endif
      e
  | _ -> failwith "not a match expression"

let get_match_cases = function
  #if OCAML_MINOR < 8
  | Texp_match (_,cases,_,_) ->
    #else
  | Texp_match (_,cases,_) ->
    #endif
      cases
  | _ -> failwith "not a match expression"

let get_attribute_name = function attr ->
  #if OCAML_MINOR < 8
        Asttypes.((fst attr).txt)
        #else
  let open Parsetree in
  Asttypes.(attr.attr_name.txt)
  #endif
