(******************************************************************************)
(* The interface to the compiler                                              *)
(******************************************************************************)

module Arg_helper                    = Arg_helper
module Ast_helper                    = Ast_helper
module Ast_invariants                = Ast_invariants
module Ast_iterator                  = Ast_iterator
module Ast_mapper                    = Ast_mapper
(* The Asttypes module has no implementation, so we cannot declare an alias *)
(* module Asttypes                      = Asttypes *)
module Attr_helper                   = Attr_helper
module Btype                         = Btype
module Builtin_attributes            = Builtin_attributes
module Ccomp                         = Ccomp
module Clflags                       = Clflags
module Cmi_format                    = Cmi_format
module Cmt_format                    = Cmt_format
module Compenv                       = Compenv
module Compmisc                      = Compmisc
module Config                        = Config
module Consistbl                     = Consistbl
module Ctype                         = Ctype
module Datarepr                      = Datarepr
module Depend                        = Depend
module Docstrings                    = Docstrings
module Env                           = Env
module Envaux                        = Envaux
module Ident                         = Ident
module Identifiable                  = Identifiable
module Includeclass                  = Includeclass
module Includecore                   = Includecore
module Includemod                    = Includemod
module Lambda                        = Lambda
module Lexer                         = Lexer
module Location                      = Location
module Longident                     = Longident
module Main_args                     = Main_args
module Matching                      = Matching
module Misc                          = Misc
module Mtype                         = Mtype
module Numbers                       = Numbers
module Oprint                        = Oprint
module Parmatch                      = Parmatch
module Parse                         = Parse
module Parser                        = Parser
(* The Parsetree module has no implementation, so we cannot declare an alias *)
(* module Parsetree                     = Parsetree *)
module Path                          = Path
module Pparse                        = Pparse
module Pprintast                     = Pprintast
module Predef                        = Predef
module Primitive                     = Primitive
module Printast                      = Printast
module Printlambda                   = Printlambda
module Printtyp                      = Printtyp
module Printtyped                    = Printtyped
module Runtimedef                    = Runtimedef
module Simplif                       = Simplif
module Strongly_connected_components = Strongly_connected_components
module Stypes                        = Stypes
module Subst                         = Subst
module Switch                        = Switch
module Syntaxerr                     = Syntaxerr
module Tast_mapper                   = Tast_mapper

#if OCAML_MINOR < 8
module Tbl                           = Tbl
#endif

module Terminfo                      = Terminfo

#if OCAML_MINOR < 6
module Timings                       = Timings
#endif

module Translattribute               = Translattribute
module Translclass                   = Translclass
module Translcore                    = Translcore
module Translmod                     = Translmod
module Translobj                     = Translobj
module Typeclass                     = Typeclass
module Typecore                      = Typecore
module Typedecl                      = Typedecl
module Typedtree                     = Typedtree
module TypedtreeIter                 = TypedtreeIter

#if OCAML_MINOR < 8
module TypedtreeMap                  = TypedtreeMap
#endif

module Typemod                       = Typemod
module Typeopt                       = Typeopt
module Types                         = Types
module Typetexp                      = Typetexp
module Untypeast                     = Untypeast
module Warnings                      = Warnings