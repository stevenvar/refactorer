open Compiler_import

include module type of struct include Types end

val get_module_type_path : module_type -> Path.t
val get_sig_value_id : signature_item -> Ident.t 
val get_sig_module_id : signature_item -> Ident.t 
val get_sig_modtype_id : signature_item -> Ident.t 
val get_sig_type_id : signature_item -> Ident.t 
