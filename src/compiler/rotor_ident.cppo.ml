open Containers

open Compiler_import

include Ident

#if OCAML_MINOR < 7
let with_name i name = { i with name; }
(* This implementation is included in compiler v4.07.x *)
#elif OCAML_MINOR >= 8
(* From v4.08.0 the type Ident.t is abstract, so we can no longer directly 
   update the name component leaving the others (e.g. the stamp) unchanged.
   However, persistent identifiers consist only of a name, so for these and 
   these only, we can just create a new persistent identifier. *)
let with_name i name =
  if persistent i then
    create_persistent name
  else
    invalid_arg
      (Format.sprintf 
        "%s.with_name: %a can only update persistent identifiers!"
        __MODULE__ print_with_scope i)
#endif
