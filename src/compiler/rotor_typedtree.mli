include module type of Compiler_import.Typedtree

val env_of_exp_extra : exp_extra -> Env.t option
val get_match_expression : expression_desc -> expression
val get_match_cases : expression_desc -> case list
val get_attribute_name : attribute -> string
