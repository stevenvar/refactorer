type t = A of int
let x = 1 and y = 2 
let _ = ((fun (x,_) -> let A x = A 0 in let y = 2 in x) [@rotor_reduce]) (y,1)
