let foo x y z =  x + y + z 

(* name capture *)
let _ = 
  let foo_1 = 42 in 
  let foo'_2 = 38 in
  (foo foo_1 foo'_2) 20
