module type S = sig  val bar : int -> int -> int end

module M (N:S) = struct
  let foo x y = N.bar x y
end

module Q = struct
  let bar x y = x + y
end

module A = M(Q)

let _ = A.foo 1 2
