module type S = sig val bar : int -> int -> int end
module M : functor (N : S) -> sig val foo : int -> int -> int end
module Q : sig val bar : int -> int -> int end
module A : sig val foo : int -> int -> int end
