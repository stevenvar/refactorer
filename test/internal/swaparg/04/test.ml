module M = struct 
  let foo x y z = x + y + z 
end

module N = struct 
  let id x = x 
  let foo = M.foo
end

let _ = M.foo 1 2 (N.id (M.foo 5 6 7))
