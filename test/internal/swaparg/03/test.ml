let rec assoc l x = 
  match l with 
  | [] -> None
  | (y,v)::ys -> if x = y then Some v else assoc ys x

let _ = 
  let l = List.init 10 (fun i -> (i,Random.int 100)) in
  assoc l 5
