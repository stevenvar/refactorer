let foo op x y = op x y

let _ = 
  let my_plus = foo (+) in 
  my_plus 3 5
