let f x = x 
module A = struct 
  module B = struct 
    let g x = x + 1 
  end
  let h x = B.(f x + (B.g x))
end


(* possible issue with inlining stuff with module scopes: *)

(* let f x = A.(g x + B.(u x + h x)) *)

(* Apparently not *)
