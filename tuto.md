# Creating a new refactoring 

In this tutorial, we show how to add a new refactoring to Rotor. For this, we will follow the example of creating that extends a function with a new argument, showing step-by-step which files need to be created or changed, and how.

For this refactoring, which will be applied with the `addarg` command, needs 4 pieces of information :
- the identifier of the function to extend
- the name of the new argument 
- the type of the new argument 
- a default value for the new argument, that will be used in every call site of the function in the codebase. 


For example, if we have the following code (in a file named `foo.ml`) :

```ocaml 
let f x = x 

let _ = f 42 
```

then, using the command `rotor addarg .Foo:f y int 3` will change `foo.ml` to 

```ocaml 
let f y x = x 

let _ = f 3 42 
```

Of course, it is then up to the developer to change the body of ```f``` to make use of this new argument.


N.B. For types or default values containing spaces and/or operators (e.g. `int list` or `1+2`), one will need to wrap these in double quotes (`"int list"`, `"1+2"`) on the command-line of Rotor.
Moreover, strings will need to be escaped (e.g. `\"hello\"` instead of `"hello"`)

## The refactoring itself 

The first part of creating a new refactoring consists of writing code that applies the refactoring. This code is responsible for going through the AST of an OCaml program (using the Visitor design pattern), and returning a set of replacements that need to be applied to the source code of every file in which the function to extend occurs. 

In our example, we follow the renaming refactoring, and thus create a new directory named `addarg` in `src/refactorings/`

We then create two files (+ the corresponding `mli`s): 

1. An `addarg_core.ml` file that contains basic type definitions, initialization functions, and accessors for the parameters of the refactoring (function name, argument name, type, and default value)

In particular:

- The `init` function initialises references that represent the arguments of the refactoring. 

- The `initialize` function checks the number of arguments and calls `init`

- The `to_repr` function creates a representation of the refactoring (such representations will be addressed in the next section).

The interface of this file is not fixed, but in this example we have adapted the interface of the renaming refactoring. This file thus implements this type signature:

```ocaml 
module ParameterState : sig

  module type S = sig
    type kind_t
    val kind : kind_t Elements.Base.t
    val initialise : string list -> unit
    val init : kind_t Identifier.t * string * string * string -> unit
    val fun_lib : unit -> string option
    val fun_id : unit -> kind_t Identifier.Chain.t
    val arg_name : unit -> string
    val typ : unit -> string
    val default_value : unit -> string
    val to_repr : unit -> Refactoring_repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make
      (X : sig
         type kind_t
         val kind : kind_t Elements.Base.t
       end)
    : S with type kind_t = X.kind_t
end
```

2. An `addarg_val.ml` - the actual implementation of the refactoring - which defines a visitor that goes through the OCaml AST and returns a set of `Replacement.t` which contain info about the location in the source file, and the text to put at this location.

Again, we adapt the example of the renaming refactoring, and implement this type signature:

```ocaml 
open Refactoring_sigs

module Make() :
sig
  include Refactoring
  val init : Elements.Base._value Identifier.t * string * string *  string -> unit
end
```

Notably, this module implements a function `process_file` (whose signature is defined in `Refactoring_sigs`) responsible for applying the refactoring to one file.

```ocaml
val process_file : Sourcefile.t -> result
``` 

The `result` type corresponds here to a set of replacements.

## Representation of the refactoring

Now that the refactoring behaviour has been defined, it needs to be accessible to Rotor.


First, one needs to modify the `src/refactorings/refactoring_repr.ml` file which contains the representations of the various available refactorings.

 The `t` types in the `S` and `Orderable` modules must be extended with a new variant representing the new refactoring and its parameters.

 In our case, we add this variant, parameterised by a function identifier, the new argument name, the argument type, and a default value (as a `string` since it will appear as is in the source code):

   ```ocaml 
   | Addarg of Identifier._t * string * string * string 
   ```

(The same must be done in the interface file...)

The `pp` (pretty print) function then needs to be extended to deal with this new variant.

## Applying the new refactoring 

The `src/refactorings/refactoring.ml` file contains code needed to initialize and apply the refactorings. 

- First, we extend `of_repr` to create and initializes a refactoring from the representation defined previously. This function calls the `Addarg_val.Make()` functor and then initialize the refactoring. 

```ocaml 
| Repr.Addarg ((lib, Chain.Ex (Value, from)), arg, typ, default) ->
    let module R = Addarg_val.Make () in
    let () = R.init ((lib, from), arg, typ, default) in
    (module R : Refactoring)
```

- Secondly, we extend the `mk_repr` function with the new behaviour. It produces the new refactoring representation from a list of string parameters.

```ocaml 
 | "addarg" ->
    begin match params with
      | [fun_name; arg_name; typ ; default_value] ->
        let () = Logging.debug @@ fun _f -> _f
            "Creating representation of \"addarg\" with parameters: \"%s\",\"%s\",\"%s\",\"%s\""
            fun_name arg_name typ default_value in
        Repr.Addarg (of_string fun_name, arg_name, typ, default_value)
      | _ ->
        invalid_arg
          (Format.sprintf
             "Wrong number of arguments for [addarg]: %a"
             (List.pp String.pp) params)
    end
```

## Command line arguments


The last part consists of parsing command line arguments for the new refactoring, running the refatcoring, and applying all changes that it entails. The entry point of Rotor is in the `src/driver/main.ml` file.

- We add a function of the same name as the refactoring (here: `addarg`) which, in particular:

1. Creates a representation `r` of the refactoring (by calling `Repr.Addarg`).

```ocaml 
 let r = Repr.Addarg (fun_name,arg_name,typ,default_value) in
```

2. Applies the refactoring to the codebase (by calling the `apply` function defined in the `Refactoring` module), and gets back a list of file dependencies (`files`) and a set of replacements to apply (`results`):

```ocaml
let (deps, results) = apply r codebase ~filedeps in
```

3. Computes the changes to the files present in the codebase:

```ocaml
let results = Replacement.apply_all results src_file.contents in
```

4. Generates a patch file:

```ocaml
 let results = Sourcefile.diff src_file results in
    let () =
      Logging.info @@ fun _f -> _f
        "Computed patch for %s" f.filename in
    if String.length results <> 0 then
      Output.print_endline results
```



To add a new refactoring to the command line of Rotor, one needs to extend the `cmds` list in `src/driver/main.ml` with a new couple *(term,info)*, in our case `(addarg_term, addarg_info)`:

- `addarg_term` parses the arguments of the new command. It also calls the function defined in the previous step (`addarg`), that applies the refactoring with these arguments.

- `addarg_info` is a description of the refactoring for help.

## Testing the new refactoring

Create a new directory in `test/internal` (here: `addarg`) following the structure of `test/rename` 

For each test (i.e. in each subdirectory), you will need to give:

- `test.ml`,  the source file of the test
- `test.mli`, the interface file 
- `params`, the command line arguments for rotor 
- `test.expect`, the expected output of rotor (a patch file)
- a `Makefile` that produces the `test.cmt`  and `test.cmti` files.

Finally, one can run `dune build` and test the new refactoring with `make test.internal.addarg`.

